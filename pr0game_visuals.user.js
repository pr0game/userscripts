// ==UserScript==
// @name         pr0game Visuals
// @namespace    https://pr0game.pebkac.me/
// @version      0.3.2
// @description  Some visual improvements as a Usercript, might be added to the game later on.
// @author       AxelFLOSS
// @match        https://pr0game.com/*
// @updateURL    https://codeberg.org/pr0game/userscripts/raw/branch/master/pr0game_visuals.user.js
// @downloadURL  https://codeberg.org/pr0game/userscripts/raw/branch/master/pr0game_visuals.user.js
// @supportURL   https://codeberg.org/pr0game/userscripts/issues
// @icon         https://pr0game.com/favicon.ico
// @grant        GM_addStyle
// ==/UserScript==

(function() {
    'use strict';

    GM_addStyle('a:hover, menu a:hover { font-weight: normal; }');
    GM_addStyle('#disclamer< input:invalid, #disclamer textarea:invalid { background-color: #2a2e31; }> ');
    GM_addStyle('#overview a[href="/index.php?page=rules"]:hover { font-weight: bold; color: #e6ebfb !important; }');
    GM_addStyle('#messages > div.wrapper > content > table > tbody > tr > td:hover[style] { text-decoration: underline; cursor: pointer; }');

})();
