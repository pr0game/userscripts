// ==UserScript==
// @name         pr0game Advanced
// @namespace    https://pr0game.advanced.me/
// @version      0.4.17
// @description  Add Features
// @author       Koron
// @match        https://pr0game.com/*/game.php*
// @icon
// @run-at        document-end
// @grant GM_setValue
// @grant GM_getValue
// @grant GM_addStyle
// ==/UserScript==

// get jquery
var $ = unsafeWindow.jQuery;

// curent page. ex: 'resource'
var urlPage = $('body').attr('id');

var urlsplit = $(location).attr('href').split('/');
var uni = urlsplit[3];

var currentPlanet = $( "#planetSelector option:selected" ).text();
currentPlanet = currentPlanet.substring(
    currentPlanet.indexOf("["),
    currentPlanet.lastIndexOf("]") +1
);

var building_images = [
    ['Metallmine', 1],
    ['Kristallmine', 2],
    ['Deuteriumsynthetisierer', 3],
    ['Solarkraftwerk', 4],
    ['TechnoDome', 6],
    ['Fusionskraftwerk', 12],
    ['Roboterfabrik', 14],
    ['Nanitenfabrik', 15],
    ['Raumschiffwerft', 21],
    ['Metallspeicher', 22],
    ['Kristallspeicher', 23],
    ['Deuteriumtank', 24],
    ['Forschungslabor', 31],
    ['Terraformer', 33],
    ['Allianzdepot', 34],
    ['Raketensilo', 44]
];

var image_owntransport ="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAACXBIWXMAAAsTAAALEwEAmpwYAAACdElEQVR4nGNgGAWjYBSMglFADeC8wP0/DDstcD9CRXOPIptNLXMxAMhwlVzN/6p5mlS1iFbmYgCQ4SBLYBZREyOby0Ar4ExlR+PCdPOAz/oAquBRD1AaAzbTHP7bTnUkmz/gMWDRZwPG5PKHrQf+H2Tz/n+Q7cn/g+z/CeDf/w+ybf9/goFvsHngMRGOR8JsNYPMA6gOTEm2/2/u7o+BPYM8/z/fLPD//wH2K4PaA+ZYHA/Ds1u0IeoOsWkNSQ+ERblA1B1gqx+SHjB39/9/b60wSN31IeuB2YSS0WD3QGSMM/5kNNg9YOHhD1N7fUh6wNwd7gHsyQiXB0DtGttpjmTzifVAWrIdXsenp9ghq28g2gOUYmI98J80jJmM0Lt+1PQAti4lhR74T3SXUjpOAQWjhygx6rB1KWniAWcSHUaOOpp5wJyIUoCaeNQD6ABXSDkEx/73TS34H5RTAca+Kfn/7YNjaBIDp5Zr/69rjvsfXVgKxvVNsf9Pr9AiPwm5x6T9D8mrwordotOo6oElU11w2rVsqjPpHgCFPC4DYdghOJoqHji1XJugXWdWaBLwgJvfY2QLQMkGpjm1rvN/1/yV/zvnrQCzYeKg5EQND9Q1xxG0q6E5FqHnANsjTA+4BngieyI4pwKuGWTYhCXrwLhr3gq4OEgNNTwQU1RC0K7YohK44//vZ/dgIASC8yo/E/RAXuVHBiqAYFrYFZJbuR85WkEGgwxMrUVEa3Be5V5qeCCEFnYF5Vb5EspYoTlV3tTwQBCt7ArJrWrFZWBwXlUzNRxPc7uC8yp8QvKq9oHSKRTvpVbID6Rdo2AUjIJRwEAVAACuTyunJI2jggAAAABJRU5ErkJggg=="
var image_ownattack = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAACXBIWXMAAAsTAAALEwEAmpwYAAAFcklEQVR4nO2aXWwUVRTHf9lda3dZu3OnoLSQUPuA+kCiookaLEhAFFssMRomKBJIFALRiBqJvvAk8qUxoqhBYkB88oFADCIi0ahR5MMPjKlSAkG+Ix8JkJC0veZspmWznTs73Z2dLbj/5DztzLnnv+fcez7uQBVVVFFFQOiFjvYSrlXoKmHn2vKwrsPW9bRrm5Xa5nOTh7O/2azIPptBcTVBj6JW1/OUVmzXNt3aRveJmXCudGnFNm3xpOhisEKPJKkVL2nFsTwCAyV8RUSX4kXRzWCCtmjVNgeNhhdL+Ip0asUjgyN8FW8HMLhUwr0eX68bSFWG7FAatM2+gMb2nLH4x0Q4+5tNT0DSu/UwhkdLNkNzNswKkDyc4fd1Q3hvbIxnAccnDzvyzMdDWHMkw/4A5A+IDVF6ttPPoL8z/DynhleESI5M9SE8NffZZ2pY3GmxpyDpcntay0nsE8aXFGeXJVmWY/wDQBNwfYBKS565GZjY+/7KJMsvKc75hPeesqYubfOhafFjFh33JJjnGns/YOW/v6N94mde4rGUFB8touu+BPOPW3T4eHpNOVOP9pJDFvuGx3gamA40+qhxDGLCCNHZGGP2oQy/+Hj64XKE8kGTZ12yk6BgeI0xiB9E52QhfdziL+N+DjO0teJlw549Ny7BfGACEKd8EN0TZMtcVJw1kH4hzOLimNci7gE1qcxkeyFrTF6VZIUhrI+G4mUtjYAh9bh7Nso6Vwi1G1OWxcySV+hWfOVVVMyr5dUCB1S5MELytFdx0q3YVno/a9OVr1gqKDf1VAotRzL84eGILp3pnw4DQ9tM9wodKRfdXFkpqPVDWOMZ1vVMK1rrZcVbXuE8MZ7duxXFgzU85hXWl21WFa30gmJHvkLpbNxysdJoPmtxNN++C4rtA9KiTT2rSRY4S8pGKd+2Bc6SgdpHQaVVwo7vP9gxs1UOsEgga1Xcwx1VwuVD1cMLQwhpcvrU15Mc/DSNzpX3U5zM62ULtXdhYkzu2h+kOJ1v39IknQF77T70Pbyoll35Cjem6Wl2h3KVJDw6xnyxJd++RbX8NFDCfdiY5jVD7ykNf0WhbeZ42bYhxeKilU5KML47/37IRncpdlJhdCm+6dct2XSNTzCuFL0jD2f4zdPLQxlLhaAz3OFVR8vcy52DFY2adSlWG8J6MxWCe73az6a1Kd4BritJ+cg4U4xzJItWIoau51EvWy4q/m2AyWGs0bQ5zSeGOdKJKO95dD2NWnHSy5ZNadYDo8JYJ94Y44nzhoW0zfdR3OjJGlrxo5cN5yxODI/zOBALa71blydZbiAssrWcpLNkbb4wrb80yRvALWGPSNv21nkO9HrlB7loK1QLe0mAMPb0rMjuDF9C9iwJfVTcOCzGrFOWz02/7GmLNqPxA6x3sweUeSvpUxYHbJgF/n90KbhbLrfOCzGzp7MpS2e4vVjCeih3mlJP7r51L+/uooyIA1OmJXg+AOkerfha28zWN3JTIcJy2mfLRcXOQhfisnZrgufEljAPKr/pf5t4+rRfePcn/6exjZPfgunJhrHrWdm3kX3WlBbSsn923cDWqD5q2V/Ht+5NZZtrQ6SoBR6SFmxpkmUBQrxowrJf3dTjuGFcsQ/WYpBtIhw5wTel2XBRcSYswqJLKijR7ZK9N6KbyoJocMPMkTD/KMW7hzL82q+1DEBY3pGuZ22K1W7Kcdz9WrbUUyzibrXT3jttuC3G3DdTrPyuji2dFntNhOX6U56Ru9/RMebmTCvaXZ2Dwqt+Yd7kfhUwI3fc4pOWckcyM9x3m6JIOWGjxm3IpQhp8SHc4j4zouR+djBBlzBKvVrhDPCzpasezv+NcBVEjP8AX65nKt7spd8AAAAASUVORK5CYII="
var image_owncolony = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAACXBIWXMAAAsTAAALEwEAmpwYAAAEn0lEQVR4nO2YX0xbZRjGazTGJUa9M1GWXZnFi10a74xsStzYOrPZ4Rg9kRD+DLIGKNQcurYMBhuF9tCCCoNACwJax9ZzhkRFdsqFZmVRl2XOIH96ELZktpXCyKBz5TGnTUl7aFkv1vVgeJLnpufN1+f39v2+fD0Syba2ta2kCZOXd8JFXwTHLIVMX8aMfbdkC4X3gmMQZRf9D/9MInYh1Pno8OsQjE0idiE0NrEBOHpRsrUBGJ9E7AJHX9pkhL6WiF2Yse+Os4m9mBtOk2wFYW44jd+w/MyHzAxi6sobqc71/1D2aOMuOUu1Ew7KTbDUg+Mj+qtSu7aFf4Z8ixQF1nEUWf0osf6Lmr4fweqee9yapOwD8E56eIKljstZaolwUAj7EK1Fel8ZegzUORRY1lBgRZSVvavo/Ko43pqYGdp1/Rs9/BOD/CbvTFr4nDFjCcEa1yLDEw4j9g1UBAHmFRemN4QPu9AK1PVx6Bp8az34NP0qXLQJHL0atdnv2V584uGJMWMG4TA+ig5PIeuH88Hw+/sr/XHDR7rEClB9o+CYerjo5RjH7JP/BWQ/G3YQDooThuctpXVBgLKOM5wwbCCvC6sfm4ECy0YQLjr47NgFdJJ5ydkDBGvIjRWed4ZNFQS4qjFNCEOuZpngebcKS4fO30BV7yBOWgMbAFzMTczSsqRuYsJBjcQKnz3aFAy/t1+59rCwe8MILWXWBwHuH25U8utA1ZuGxv4RlPU8Asf8Co45CuAZ/llyAVjjYiyAI9/WBgFO9Kg9wvBr+RZ409XwpKvXIGt97KYkkwwwGwvgwMWqIEC7Xv+nEMCf/Vmw+779Z+cS+Q4yuQAUKQwvZ43YO6BEel85/j7V6RYC3Jc2hMZH2tCZcgCZzfasnKWqCYfRs358fh86Pg93qVb+KuoQnDIWePdpQgAyw56UA4Qlu6V7PoelPiRY4+CRodqV9wcq/KT6jPsjRRVKTunQVaLHH0VteChvC4ZfyKheliQo8mldJYQ6pwgBRFpbVI1xWS3untBfEz0AWVo5maVQRwGEnafSBvp7Wt8WNUCuonhKX3EWx+JAKLQ1KzbLF6+LFiCn+OSdPQdzUFpOQl6miQlB1te7u9nuF0QJcKyg0MsD8D5apEA+WR0TotbQMCFKAGlu/nIYgLdSo5pS6GoexIJo/twwJDqAjJw8fyRApbrUNtDVtPPTujqPECBXpcXvP30pFxXAO1mfBCIBdLryCv5zm82wo75Zfzsc3tDaBN/tS/ztc0o0AJ5rppcWxk0rvnEzwnb/0vJaZE1bR/PIjbGeyLv/PdEA+Jzm05HhfU7THWENODpP8GpxWBQAnmD3zd5oAPNpYR1cdIcAQCMKAJ/TrBGEX1j4jXpFWAcXcysKYNb+XsoBPIl2f9r2MlxMIOLvY4D/LOUAvkS7P8NkCN443Iy3Jvm0ABLtPi9wjE4A0J5yAF+C3ecFjv5OAJCbeoBx83wi3ecFF+2OBhh6UwwAc+Hw/CjxIxWvFi56LqL7d8OvUFIKsHi9+YDPaeIWnKbJRWfLwc1qwdkz4aLnQyD2zM1qyVTdhba1LcnW1X9gBvdOqB02DgAAAABJRU5ErkJggg=="
var image_owndeploy = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAACXBIWXMAAAsTAAALEwEAmpwYAAACAklEQVR4nO3YPWsUURQG4FdRRDQWolZ+YGMhCIL6B2wsNNpoGyz8CyqIYGGjpaiNioiFAStBWHZzzjggawoJYbPnXTfEj8LKDyKCIKIxIzNBESU7s2vInCvzwC0H7jvn3N0zF6hUKv854VcIP0D4AmoC5XUoT0K4HUFQJj1WF2LnEXV2INAAycKy71A+xFh7P8IMwIUlNg/hXTRaWxBkAP1VkVlEnRMINwB/VuQaJiZWhxtAs2oomt2hgAMwgfAJGq115QSI41WIpzfhUXsfxEYgdhvCNwO0U638dvo9lNowxB73GeIq3JH2YShf9hHkONxpdocgdq/wT2zaki4pzxY81LfglvJisX/sqT1wSzhaoAqjcCs9E8pXOSHmfI/kYkcKHOhzcE3YzDkLhGsRj+ZWIZ7aCrfS0UH5NqcKI3BN7U5OFa7AtfQCoHeAOlwTO5DTQs/hWm1yc06A93BBeLOv0brY4Hdj+QLUZtZA+XQJNz+J8fG1WFZ12wbluyXY/CwarZ0oRcSDEH77hwBzGLNDKJXamYEDiJ1G6ZJkBcTuD9A6D7JnXYi5HmKd4m+e06jNbIAr8mwXlB8LtM0nKHfDJekcyz4hF9/8vJ971MUIL/cIcAnuXUhWZkPb35uPskuyINS58Y/v5NfZnBSUBvdC+BlqX7IpNUhqp7JVqVQqqR8JqHUs9ZprfwAAAABJRU5ErkJggg=="
var image_expedition = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAACXBIWXMAAAsTAAALEwEAmpwYAAAHVUlEQVR4nO1Za0xbZRiu0x+6P2r84+Yc59JdnCa6RH/opot/dVmiTpdojNHEy0ym/nJzhpaOglwEBu05jF2zLQwHY4zgwi6yjbFlbGxc2wK9Q6FQru13RuaF5DXfd3raHnpKTwFZYniTN4H09HzP897frxrNkizJksxbtujhMZpHb1Ic0tGccJbmBRvNCxMUh/7Giv+mOcGKP6N4lM6Y0Wb8Hc3DFm1ZkKV5tJ/iUIDmBUhFKQ4N0xwqWs0HmUUHvqp06jmKRydpDk0nAshme0Crs4I23QpsRg+wRhewOf3AFAwDw6EoER79Q/Po+POmqZWLAj6NQ99QnICSWZjN7SfAtemWeM3oUfCIgCgeffWfAV9VAE/QHDqdSpiw+YOKBNgsV+LQ4tGptGPw+IKCTyuafIrihaZU4xyHixIBumQi2XcbmbKJJxfM8hQnXI+8vHgMmJx+YPfZxRjX2UBrsAOT6wPGHJJ74Fd/HHgmb0AZtDkE2kwnMOag+D8n3FpRBsvnTUAKGwyOzXYrx7SkehswhcMxiRzzvM5KCCXOmQExvDJ6gDFNSpWqfF7gGR7tFK2BgDXY4wEbekWPmIPA4Hg39ACbaY+CMrrCoLqBKRpVDjMOAWt0znivHWizWKkoM/pyTuBXl91fQfFCiByS0x+fiLiSzAiZOKvm9AGb5QbaFJy1WmmVEj3bG05qITSnEkvqPD6kcAS0uvgDmKKRlBI6kWr3ORKGZMwZx1KzPh9kSIPhBOJOxUoS04zokknxORxS+8dSI8EJBKgiCUOPeA6HpllzUKuaAB4PSOgUBhJahw4nGgGvjzas2BxQS4D9pY98d53eCsea/OAfC0FuvU/0QsGQ1B8KVIHHQxaZU3B8ZipbXyqHzP4xkqCyz3S2FMCHSKxL4E/fGYKJECLqGw3JDIIxqRoAKTN6S6zLQcXYT6YkucMAv6vxQv2tdqho7ALT5R7Q/e6Cb6v74cOKIXj7yBBsMPYqgsd69Ea0h0hlNe1A6PXkBHiULnbRoZTBk8NyfREC75wMwJErNvijuVVRL99qhTONbdBi98vAn2kZJqQi78wP9w8O/awi/oUasQl5UgOvs4mlLza5sSF4gVi8/FpXHIGG223g7J8dvKykcsKZ5ATw4oEJ4FEhKXArMHk+Em5Jy2Upgs+rBqC6qTMh+DbXUBx4QsAQyYMuNQTGCQG9LXm85yaYa2bR9WUIdtX0g30GePeAn4ST4ln6bskDo8lzgEN/EYuRZWR2AmosH9d5SwUo7xBk4F2+QeIRnBPr9UrhaZVK6Z+qCcTNJzEDG25YhMAcOq+pWQ7e6fMT8FJebMrpmh8BKYSYknGZF5j9o7IBbC4E9jZEgWOtaB6G70/Ik3tbSWfiEOKFkeQe4JElAhSX0jCJWAIkxBTWwljFPQAnbE69HT6rHIBPzk5AYDIKvvTqIKzVWWBDhgWqrkUJfHE4ngAuKGEPdKogIFTLwJBhzhq1BDf7BCopLp3nbnQQUFfutMHQ6DgBPjwegj3V4uigDSsGLRHYXd4eTyDbI723MimBXXWjh2eCwZ2QzcK3DBay56oNmZcPBqHgYi9YnF4C3j8yBh8fjF/01+osUN4g5kFeTTwBqZFRPNqdlEBzT989tlTejCJaNComsVITM7oU9wPcyKpbh8Hi8BBPfHoIX7N0x1W5HbzoraOXFAiERwnmwNSrSQlYnO77P10ITCayKl4ZE44RCfZdTKLoUi8BaDjvCI8FAhkGcTPE312js8ChC+1Q29QqD59M8XmKQ30aPSxTRaDD4Zn+qGo88fKdqDfouyNLyMbDIdheGX3HxkOTUHuzHX673invC/uiHX9rcSf8cKJDbpTCgNTEsjRqpMvpbrM4PdBu98LeiwFYoxBOszW5zXk2yKh1wLkWL7zEyyvXjgo/qU4y7+Qor5RE8YIUrv/4NlAVAYvDk4kJSNpo7YOshmF47/Q4bD4ehBfKBFhndMArRiu8kWuFbWYbfH2iF3Lr7HC+xQVdDg9c7XDDplwbyZdkic4kmnp1uHSHtzsOFasCTwh4PM9aHJ5gLIlUtP6eixATh7De5ATMIUWP4guB8DNjK02hZ1QTIGHk8n5gcbinUwVvuuiAFw0x3RtPqmrmI6N4/RJRnLjSWG5G2zVzkU6X990up3tQDfDKmy7Yapq5Wloj5S+pF/Kj2xdO6ugNHyrRzEfuDg4ub7d7dlbe9fkbuvrgXq8X2uxeuG7tg1O3fbCnshe25Cu4H19kFaZw7WKajFheAk9xQt2C/QgSvh+ti3O9QlNj8bih0vJ07Ltwh5fChhNq8ZmahZTwbYV51tjFE+qMoS9l5VCxphIe1fxXgpMKj7XRzhwANtMpxny46cxFKQ4FGA69r1kMWc0Hn8YJRnPoQYTIjGU+BYs/wBdp+DcIzWILvnTFLR7PKamDR16KF4z4Elnz0EUPy2hu6jU87lK8UEXxqANvdng9JSsq/ptHHeQzDv1IpkqARx427CVZEs3/QP4Fc3LrNYjFwbYAAAAASUVORK5CYII="
var image_ownharvest = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAACXBIWXMAAAsTAAALEwEAmpwYAAADU0lEQVR4nO2Y70sTcRzHj3qwJ/Uk+xuEop4YbGI//NFtQ8m7Mle7S9G6pZRRFCj4C8F0mqkr3fIXpEwoXRapSImjTVcpOtMkIftBZYZa6ANN8El94/vdD7fzCrzuDmX3gfeD+97xeb9fd/fZvhuGySWXXKFVoF8BxBQmAcCyaABOxZIUAO0iArSJD9C3MwxBOBVLAgZfAU5FB3Ds2I1JVUoNuarSkkAIKTXkKru/mmIy1bQBsCUgAPFdMAAtMR/YW6Njdqlp5ofYAB+geXfvJHgxPM9L3b2TXgDyfWBvNcU0coUXGIB8Bc0fdI3yBrB1un2v0Kivr4Y2HMAp5pcEAIQTmlttLt4AVpvLC0A4YM+ioqJtOM0MBQbGaYNbp9NtFyz4GgDZCc0bW+28AZqsfZ450JCPuQYXPQl9hkrw8B4AohWa1zZ18Qaoaer0AhBWrsHFaUODKOG9ABZoftNs4w1QYW73PQEzDMsKvxBNZYj3vaDUkmXQvLiyhTdAcWUzAohKSLrLHlwNbcgQLbwHIDEXmueVNvAGyCutRwBHjuun2YMLB1pkACILml8rNPEGuFpYjQCiT56RZnCDAchUaH4xxxgU6vnwHLj36COosEyAghtukF/uBsaacdDS9g70D84GXXshx4gAYpJTAu9+vejhPQCJJDQ/e6XQH2hgaA5U1U2AXOMIp0pujQG765v/+nOXCxBAbHKqNIMbWCotGQPN6cxsf6Dm+1Mo6HXTGHjY8xk4X84idfR8QmvwnKnxjf96OjMbAcSdSpNmcAMrUnssAponpV/yByq9PY5CPnXMrHvfnzybQefyykbAwOAcWjuRloUAjp5OF2dwcYpJwCnmK3s/AtdiklIYaB6vZ/why2tfo5B9zrXXxKdehwcgv3wEuIY8a/F6xgfwG6eYeS4fNX0+/n8A1oWH2ncwFoRHRPq1V3l4w1voPapDQT32R8Vxbt5w2jDNG+BvO0JoaKkzgZXFKWC+U42ONwrA1UMt9A70XwA/F6cAWP0ClhfeouON/ozk6qGWEgDeNRig1lLFG8DM6iEpAFt8AMJZEgTAXhIGNrOwkAMAPP/rEasPJgP0b7EnYN9kwkIOAMhDHCZ/CtlDegbsWw1ALrnkkguTsv4AUnhMN+7CxdgAAAAASUVORK5CYII="
var image_marked = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAACXBIWXMAAAsTAAALEwEAmpwYAAAEI0lEQVR4nO1YW2gcVRieCoqg1Bcv2Cf70D6UnMnOmRK3rVgIbWnaEp8UrfpgrVUpmNZK0yYz2aQRVNIG9KEqVRGNEELB5KErM3P+3SQPlspCEpMmCG2SJZjLNomU9sEL6Sn/SVY7u7O7M7uzSYX54H851+871+8cSQoQIECAAAHuIzDGPo1EIg+sNQ+pQle2yZrShUEaaNhtPQDgjLGL8Xj8YWktUanRl2SdcgzSpL7oRQAsi4BoNLpe+r8KgOX41TTNDdJqQz2iPkg0pf5fAbrSrDaoTxchgAPAuGVZm1dn1COVz8ia8i3R6e00+YwYJY30KAr0IIADwLxlWds8kcnRkC3i8Tifnp7mi4uL/FIiyqvObHcinRUHP3+dT/w+IeqNjIwU7GclbjPGanwVMDQ0JEgMXBvkW1vCrsin440Lh/nCwgKfn5/nfX19rkQwxv4BgEO+CZiamhICjnd8kEXwyDfvcmPA5JfHLvPz5hc83PpcVplLiaioPzw87HYWUMQdAGguWUAsFhOjhwTCrTtsxPa11/LUjZTISweSzRRwrOOEyJucnPQkgDF2smQBOO3YeXI6mUWsvrPBRj4du9tqbOUOtL8g0ufm5twK+AsADvqyhHp7e0Xns6lZXtmkZiyfdxwFHPrqLVu5cOsOkY4z6WLkb1mWtVcqB4hGJ2yzoNEl0hjaVUxb4CxgNhaLqVK5QDSlxeGk+ZPoirYlsuXREgWMm6a5qWSSnPN1APCxU15VpGq9rCvXnI5LotE/ZJ1+iebOqwDGWMIwjCdLJt/V1fUQY+wHbDRXmZAW2kR0Op73/Nfoz4WcKfxHnvli5gzDeAQAoumG85VV69XHiE4/w+WTR8SSrNMmSZLW5RHQkUgkctoN12CMPYXTeO+0uqmnRtTHiaYcJzodySWE6MqZHH224XItmbxhGBsB4LfMTeW1nQo9tJPo9CcHEXcqG9QqqRzAIwuPLqcjrdg2iUbrHDZ4p7/Ml9dgNQDcLHSxoBFzshFo0pwuMYz97bW2snvP7hfpqVSq4EXmirxlWa+uXNcFG8Tr34nUnrZ9OQUc/vptW9mtLWGRjoNRsgDGWB0ALLk1VclkUnT+3nfHsjaoNciyyKO5q/5kj930nat17YV8F4APEey8+0pPloCdH1Xz7l96+NyN5Vm6OnGV133/fla5U52Nrt2o70uov79fmDCMV86/5nhU4hLZ/uHzznnNz/LB6ysPooEBfwR42cQYY2NjgsDo5CivOXfA9WsMhf14pVvUnZmZEU9T3wQUe4zi5SXrygWiK3/ntxOKWRFRZKncMIq8yEKnQ0/ImvIy0ejFe4j3yE2hN/HXQlpNsCKtBAI/s9IC8JNLWisYHszcfSnArZ3OBNrm9Oeu27dAWbHyoDlb3l4CBAgQIEAAyRPuAtYGbKxCjk1kAAAAAElFTkSuQmCC"
var image_ownespionage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAACXBIWXMAAAsTAAALEwEAmpwYAAAEDklEQVR4nO1YW0gUYRSet4oub0G9BvUY9FSvhXYD2bJGDdM5Z2bSUCzsQlZ0gyiJqOx+UysqDXooclej+1NlpVJC0UUjH6KifKjdbVfhxBnX2X9mN/Pyz6awBw7M/jvMfN+c75zzn19R0pa2tA3L8vPzpyDiKkQ8h4jNAPAVAKIx/8priHhW07Q8XdcnK6PFCgoKZiFiNQAEEZEG43wvAJwHgJn/DbiqqhMA4CAA9AwWeBIiHJ0DmqaNTyl4AJgJAK8SQK2rIKOyjoyaFjLqO8i82W05X1trlXXWPUnIPC4qKpqeEvC6rs+JaToOYMMuMmpbyfSHBuXGpXbSN+91R6OrsLBwdiq+fBy8YZJx9DaZDcFBg7e9IUjG0SbrGSIJTdOmeQKedQoAbTb44lIyLrQNHbjbL7SRXlwqRuM555d0Apyw4pfnFycDtO1BmM60ROj66x4KvO9zvua17Q/DySV18SWhsUaMxB7ppVKsNpZsXCDK74bo8qso3enoGdCvtEep/E4SElWNIoGfUqXEdb7/4frG3Qmar7gfpsD7PvC33kbp8NPftOlemNYGQpbzNa/xf3yP/12UKh64otEQJH3DLpHESWkdVmxSbumUNIWp8UPf161ujVi//6Z3/q+6LWLdy4SLAq4o1LaIufBLSseObQ9idX5rAigGcellhI49i5DpApTUAyE6/jxCV9sTCZj+kKNPAECuDALnbPlU1o286vzD9f1XRAJnZBBotpO3psVzAkbNC0eHHjEBAPhmE7j20XMCZn2nSOCLjAhE7ATmvY3XBG52ixL6LZnAD+8J3Pgul4BDQvWd3udAfYd0CcWTuNb7JDZlJzGPgTaBFJdRRDw9YgI8w9p9oGyL9wTWORqZOmICJSUlk7itx2XU6p3+a51bCX63IsN4ALejUL6DTP8wBph/uWszh4hnpYAXJrFofDvdJF86VQERPJfuGdIIxEgccIySEqWUZKDZr3gxUnJZs6VUXPrXqWxoum91jJQA8K6srGycdAIxEtMQ8ZMjElWNwxvq/cE+2eiG+4ilFxELFa+Mjz749MDx0vXbyDjfPKRqo5fvHOjQq9dTEog4FQAeuV/MfYIbEW+JxYMtk69rXlj/8T1JTujeiEUCrTXs9al5cod70VinfHog9ohhOFebfbFnZfeTAEDK9Kk0b5GPlq7IOaJ4aZwXPIAPkcgvnrbcpRIAsjUNogx+7iKf5SkhwcZdk2dY3r8AwBPeSca+MPsXXgOAU4iYM1CHXbI8Zw+D7idgkVi8jJZk5x5Sxopl+FYeTyBhRSL3sDJWLCNNYpRYRjoSo8QyfepJd2IvyFrxSRlLliHIaX5W9mdVlTTwpNIWLlNPLMha2ZWZuXri/8aStrQpo9D+AKMobrmj90reAAAAAElFTkSuQmCC"
var image_attack = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAACXBIWXMAAAsTAAALEwEAmpwYAAACgElEQVR4nO2Zy2vUQBjAcxb1bxBvgohgZ1YXdd2ZrVuoLVYr9VHxBaVCQdGLvVbwIh69CYIoPg5e/AMEj+J/4AN8QNtt0WUeyRpJRrJV43a27GbyZZKFfjDXL7/fzDffZBLH2YyNY7lS2bo6grc7gxZiGO0VFL3hFIeCYsUpescIPuQMQrAqOiwo4hH4/4NT7EuCJpwiB6uVDgqK2Hr4gZBgPeALLcH6hC+kBEsIXygJZghfCAmWEj5XCQYEL/KQgIYXNiWyghc2JJLAe/PX1frwbl3LT4IlnHnv5lVd4MZsPivBDMrGnbusCbhzl+yXEzOseXdmWheYOWd3T7AUG9a9eEoXuDBpb2OzlN1Gnh3XBOSZcaNcIqkERKuUJ+u6wIm6cT7RrwRUn5djVV1g7EiqnKKXBB8e2t3tJmU06mVNQBw9kD4vXZOIJloXIPgVCPyfoYIgpg8CsLwikiD4bTcBASrguf/4Q9cFFRAUq5VyeVuHgCC4CfmAsPkjFmh+h16BQB3bt6VTgKKnoAKN5VigsQS7AgS91krIq5R2CIIbUA8Jvn2Jt8DXz4ACiHNS2rNRG93FKV4EEfj0IRb4+B6qdCQnuNrrLACRkFOjSk5PrI2pUTvwWayEtZmHlpCTI+rng/vKf/JQuVdO24VPKyGP11S4uhIfZL985c6etwufRqK1MK+9SvjPHtmHN5VoX2jCsEOgdfd2PvCmEq17d1S4tNg+kf3nj5Wo7c8P3lZ34lnCZy3BbcBnJcFtwkNL8DzgoSR4nvBpJQoBbypRKPikEoWE71ei0PB/I7otdb/ZIRb9BHcGITw6tFNQ/KL9oYDgJqf4ZbQ6eXNthpNB/AYeXF9CbPj24AAAAABJRU5ErkJggg==";

var image_left = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAACXBIWXMAAAsTAAALEwEAmpwYAAABUElEQVR4nO2YS04DMRBEDVzEPiXLZJkQCNm4pYTfFnE89wEcgbJAiC6PR26PHflJ2UQq6ZWn7fkYMxgMBleN87x2xBvtjAoXkXj5bbQyKvwRmSTkZmRUEETi9/8lMyqIIsTb7Iznh37lKezakAer6ISM9eGxDXmwiqI8hX238s6H5ybk0QiAwoc25Ck85cvz0cR406v8qRX5ff6o8Uu/8sSvZhVvF5dHJwdY+bd+5Ynf25CncJDmF2zYj0bkWTz2QOFP8xXvTE0shfvcAnJmgQIJIXGEQPG6I5QUAptYylTfxColah+j6VWVb2TgStS9kamU8JUfJZJC4GEObOzTNZQ4tlUCvNDAY3kJgNCu5InWTQlb+6U+JYQ+q9gZI6gK2KTbklevnxK+8qfFCULrkhlV/hOyFFalM6r8FpoqYmdkVPl5Q8scAzcjMxgMBmYRzmqJuzMDxZl9AAAAAElFTkSuQmCC"
var image_right = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAACXBIWXMAAAsTAAALEwEAmpwYAAABTElEQVR4nO2YwU7DMAyGW57EfkqOG4OxXtlmTxx5xfgBgkA7IDRHsyWnyZRP6tHS96eO02aaBoPBoHmQZIssS3RNCFeRfH2WqJoQ/oncJeSpCQNZXm7IFIWUACuGINmZQziCh4Ikr92HAJK9o53Mby8UYHl3hDC/vVCA5NB9CGRZrELA8tZOiJxn5HQ0h3DsozhynoHS6RFCnB3tZB4GsSFYqP8QJOxop0NTIZDl4jixl9ZCfPYdYpOfkNOXSSjnGSl9tBGiEAA4bawBQKup3UIlEVRaaA35S7fyoIzRkggoY7S+vHKQFeWVgwzqr/ztT4le5E+Ottk3smHTsU/5yTc5tB8aqC3vmRzaLyVUl3dsvnbkyd6/2rVK/Z4n+ypqF1tryO/6lWe7iHa5W13+B+D0bBXx1ITyV+heEU9NKL9tQbKNrhkMBoOpO74BdW67NWkcCZ4AAAAASUVORK5CYII="



function timeConvertor(etaTime) {
    if(etaTime == 'Infinity' || etaTime == '-Infinity') {
        return 'Infinity';
    } else {
        let etaDays = Math.floor(etaTime / 86400);
        let etaHours = (Math.floor(etaTime / 3600)) - etaDays * 24;
        let etaMinutes = (Math.floor(etaTime / 60)) - etaDays * 24 * 60 - etaHours * 60;
        let etaSeconds = Math.floor(etaTime - etaDays * 24 * 60 * 60 - etaHours * 60 * 60 - etaMinutes * 60);

        if (etaTime >= 86400 * 100) {
            // more than 100 days
            return '~' + etaDays + 'd';
        } else if (etaTime >= 86400) {
            //more than a day
            return etaDays + 'd ' + etaHours + 'h ' + etaMinutes + 'm ';
        } else if (etaTime >= 3600 && etaTime < 86400) {
            // more than a houre
            return etaHours + 'h ' + etaMinutes + 'm ' + etaSeconds + 's';
        } else if (etaTime >= 60 && etaTime < 3600) {
            // more than a ninute
            return etaMinutes + 'm ' + etaSeconds + 's';
        } else if (etaTime >= 1 && etaTime < 60) {
            // more than a second
            return etaSeconds + 's';
        } else {
            // 0 or negative time
            return '-';
        }
    }
}

function datetimeConvertor(timestamp) {
    let date = new Date(timestamp * 1000)
    let day = date.getDate();
    let month = date.toLocaleString('default', { month: 'long' });
    let year = date.getFullYear();
    let hours = (date.getHours() < 10 ? '0' : '') + date.getHours();
    let minutes = (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
    let seconds = (date.getSeconds() < 10 ? '0' : '') + date.getSeconds();

    return day + ". " + month + " " + year + ", " + hours + ":" + minutes + ":" + seconds;
}

function isEven (number) {
    if (number % 2 == 0) {
        return true;
    } else {
        return false;
    }
};

function arrayLookup(searchValue,array,searchIndex,returnIndex)
{
    var returnVal = null;
    var i;
    for(i=0; i<array.length; i++)
    {
        if(array[i][searchIndex]==searchValue)
        {
            returnVal = array[i][returnIndex];
            break;
        }
    }
    return returnVal;
}


function Sleep(milliseconds) {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
}

function GetPlayerId()
{
    let cords = currentPlanet.split(":");
    let gala = cords[0].replace('[','');
    let sys = cords[1];
    let plani = cords[2].replace(']','');

    let url = 'game.php?page=galaxy&galaxy=' + gala + '&system=' + sys



    $.get(url, function(data) {

        let $data = $(data).find(".table569");

        $data.children("tbody").eq(0).children("tr").each(function() {
            let value = $(this).children("td").eq(0).text();
            if (Number(value.trim()) == Number(plani))
            {
                let tooltip_content = $(this).children("td").eq(5).children("a").eq(0).attr("data-tooltip-content");

                if (uni == "uni4")
                {
                    tooltip_content = $(this).children("td").eq(6).children("a").eq(0).attr("data-tooltip-content");
                }

                let playerid = tooltip_content.substring(
                    tooltip_content.indexOf("playerid='") + 10 ,
                    tooltip_content.lastIndexOf("' onclick=")
                );


                GM_setValue(uni + "playerid", Number(playerid));
            }
        })
    });

}

function HeaderProduction() {
    let current_metal = Number($('#max_metal').attr("data-real"));
    let max_metal = Number($('#max_metal').text().split('.').join(""));
    let current_crystal = Number($('#max_crystal').attr("data-real"));
    let max_crystal = Number($('#max_crystal').text().split('.').join(""));
    let current_deuterium = Number($('#max_deuterium').attr("data-real"));
    let max_deuterium = Number($('#max_deuterium').text().split('.').join(""));

    $('#resources_mobile').children("div").eq(0).children("a").eq(0).children("div").eq(0).remove();
    $('#resources_mobile').children("div").eq(1).children("a").eq(0).children("div").eq(0).remove();
    $('#resources_mobile').children("div").eq(2).children("a").eq(0).children("div").eq(0).remove();
    $('#resources_mobile').children("div").eq(3).children("a").eq(0).children("div").eq(0).remove();


    let eta_metal = Math.round((max_metal - current_metal) / Number(GM_getValue(uni + currentPlanet + "seconds_metal", null)));
    $('#resources_mobile').children("div").eq(0).append('<div class="custom_timer" style="color: #765FBD;" data-time="'+ eta_metal +'">' + timeConvertor(eta_metal) + '</div>');

    let eta_crystal = Math.round((max_crystal - current_crystal) / Number(GM_getValue(uni + currentPlanet + "seconds_crystal", null)));
    $('#resources_mobile').children("div").eq(1).append('<div class="custom_timer" style="color: #765FBD;" data-time="'+ eta_crystal +'">' + timeConvertor(eta_crystal) + '</div>');

    let eta_deuterium = (max_deuterium - current_deuterium) / Number(GM_getValue(uni + currentPlanet + "seconds_deuterium", null));
    $('#resources_mobile').children("div").eq(2).append('<div class="custom_timer" style="color: #765FBD;" data-time="'+ eta_deuterium +'">' + timeConvertor(eta_deuterium) + '</div>');
}

function TimerUpdate()
{
    $('.custom_timer').each(function() {
        let value = $(this).attr("data-time") - 1;
        $(this).attr("data-time", value)
        $(this).text(timeConvertor(value));
    })
}

function Menu()
{
    let step = 9
    if (uni == "uni4")
    {
        step = 10
    }

    if ($(".message")[0]){
        $(".message").remove();
        $('#menu').children("li").eq(step).children("a").eq(0).css("background-color","green");
    }
    else
    {
        $('#menu').children("li").eq(step).children("a").eq(0).css("background-color","");
    }

    $('#menu').children("li").eq(24).after('<li><a href="/index.php?page=rules" target="_blank" style="color:#ff0000;">Regeln</a></li>');
}

var Imperium_min_array = [[null,null],[null,null],[null,null],[null,null],[null,null],[null,null],[null,null],[null,null],[null,null],[null,null],[null,null],[null,null],[null,null],[null,null],[null,null],[null,null],[null,null],[null,null],[null,null]];
function Imperium_min_level(arrayid,planiid,typeid)
{
    let level = Number($("table").eq(0).children("tbody").children("tr").eq(typeid).children("td").eq(planiid).text());
    if (Imperium_min_array[arrayid][0] > level || Imperium_min_array[arrayid][0] == null)
    {
        if (level != 0)
        {
            Imperium_min_array[arrayid][0] = level;
            Imperium_min_array[arrayid][1] = planiid;
        }
    }
}

var Imperium_max_array = [[null,null],[null,null],[null,null],[null,null],[null,null],[null,null],[null,null],[null,null],[null,null],[null,null],[null,null],[null,null],[null,null],[null,null],[null,null],[null,null],[null,null],[null,null],[null,null]];
function Imperium_max_level(arrayid,planiid,typeid)
{
    let level = Number($("table").eq(0).children("tbody").children("tr").eq(typeid).children("td").eq(planiid).text());
    if (Imperium_max_array[arrayid][0] < level || Imperium_max_array[arrayid][0] == null)
    {
        if (level != 0)
        {
            Imperium_max_array[arrayid][0] = level;
            Imperium_max_array[arrayid][1] = planiid;
        }
    }
}

function Imperium() {
    let planetcount = Number($("table").eq(0).children("tbody").children("tr").eq(1).find("td").length);

    for (let i = 2; i < planetcount; i++) {
        let cords = $("table").eq(0).children("tbody").children("tr").eq(3).children("td").eq(i).children("a").text();
        let seconds_metal = Number($("table").eq(0).children("tbody").children("tr").eq(6).children("td").eq(i).children("span").eq(0).text().replace(/\s*|\t|\r|\n/gm, "").replace("/h", "").split('.').join("") / 60 / 60);
        let seconds_crystal = Number($("table").eq(0).children("tbody").children("tr").eq(7).children("td").eq(i).children("span").eq(0).text().replace(/\s*|\t|\r|\n/gm, "").replace("/h", "").split('.').join("") / 60 / 60);
        let seconds_deuterium = Number($("table").eq(0).children("tbody").children("tr").eq(8).children("td").eq(i).children("span").eq(0).text().replace(/\s*|\t|\r|\n/gm, "").replace("/h", "").split('.').join("") / 60 / 60);


        GM_setValue(uni + cords + "seconds_metal", seconds_metal);
        GM_setValue(uni + cords + "seconds_crystal", seconds_crystal);
        GM_setValue(uni + cords + "seconds_deuterium", seconds_deuterium);

        for (let k = 0; k < 19; k++) {
            Imperium_min_level(k,i,k + 11);
            Imperium_max_level(k,i,k + 11);
        }
    }

    for (let i=0; i<Imperium_min_array.length; i++) {
        if (Imperium_min_array[i][1] != null)
        {
            $("table").eq(0).children("tbody").children("tr").eq(i + 11).children("td").eq(Imperium_min_array[i][1]).css("background-color","#800303");
        }

        if (Imperium_max_array[i][1] != null)
        {
            $("table").eq(0).children("tbody").children("tr").eq(i + 11).children("td").eq(Imperium_max_array[i][1]).css("background-color","#036313");
        }
    }

}

function Overview_Stats(playerid)
{
    $("content").eq(0).children("div").eq(0).append('<div class="infos"><div class="planeto">Statistik</div></div>');
    $("content").eq(0).children("div").eq(0).children("div").eq(4).append('<table style="width:100%"><tbody></tbody></table><br>');

    $.get("game.php?page=playerCard&id=" + playerid, function(data) {

        let $data = $(data).find("tbody");
        $("content").eq(0).children("div").eq(0).children("div").eq(4).children("table").eq(0).append('<tr>' + $data.children("tr").eq(4).html() + '</tr>');
        $("content").eq(0).children("div").eq(0).children("div").eq(4).children("table").eq(0).append('<tr>' + $data.children("tr").eq(5).html() + '</tr>');
        $("content").eq(0).children("div").eq(0).children("div").eq(4).children("table").eq(0).append('<tr>' + $data.children("tr").eq(6).html() + '</tr>');
        $("content").eq(0).children("div").eq(0).children("div").eq(4).children("table").eq(0).append('<tr>' + $data.children("tr").eq(7).html() + '</tr>');
        $("content").eq(0).children("div").eq(0).children("div").eq(4).children("table").eq(0).append('<tr>' + $data.children("tr").eq(8).html() + '</tr>');
        $("content").eq(0).children("div").eq(0).children("div").eq(4).children("table").eq(0).append('<tr>' + $data.children("tr").eq(9).html() + '</tr>');
    });

}


function Overview_ShowGalaxy(url,planet)
{
    $("content").eq(0).children("div").eq(0).children("div").eq(1).children("table").eq(1).remove();
    $("content").eq(0).children("div").eq(0).children("div").eq(1).append('<table style="width:100%">');
    $.get(url, function(data) {
        let $data = $(data).find(".table569");

        $data.children("tbody").eq(0).children("tr").each(function() {
            let value = $(this).children("td").eq(0).text();
            if (Number(value.trim()) != Number(planet))
            {
                $(this).remove();
            }
        })
        $("content").eq(0).children("div").eq(0).children("div").eq(1).children("table").eq(1).append($data);
    });
    $("content").eq(0).children("div").eq(0).children("div").eq(1).append('</table>');
}

function Overview() {

    //PlayerInfo
    let playerid = GM_getValue(uni + "playerid", 0)
    if (playerid > 0)
    {
        Overview_Stats(playerid)
    }

    //Events
    $("content").eq(0).children("div").eq(0).children("div").eq(1).append('<table style="width:100%">');


    var events = Number($("#hidden-div2").find("li").length);
    for (let i = 0; i < events; i++)
    {
        let event_class = $("#hidden-div2").children("li").eq(i).children("span").eq(2).attr("class");
        let event_typ = $("#hidden-div2").children("li").eq(i).children("span").eq(2).children("a").eq(1).attr("class");

        if (event_typ === undefined)
        {
            event_typ = $("#hidden-div2").children("li").eq(i).children("span").eq(2).children("a").eq(2).attr("class");
        }

        let event_subtyp= $("#hidden-div2").children("li").eq(i).children("span").eq(2).attr("class");

        let event_image = "";
        switch (event_typ) {
            case 'owntransport':
                switch (event_subtyp) {
                    case 'colorMission3Own':
                        event_image = image_owntransport;
                        break;
                    case 'colorMission15Own':
                        event_image = image_expedition;
                        break;
                    case 'colorMissionReturnOwn':
                        event_image = image_owntransport;
                        break;
                    case 'colorMission16Own':
                        event_image = image_marked;
                        break;
                    default:
                        console.log("Image needed: " + event_typ + " "+ event_subtyp);
                }
                break;
            case 'ownattack':
                event_image = image_ownattack;
                break;
            case 'owncolony':
                event_image = image_owncolony;
                break;
            case 'owndeploy':
                event_image = image_owndeploy;
                break;
            case 'ownharvest':
                event_image = image_ownharvest;
                break;
            case 'transport':
                event_image = image_owntransport;
                break;
            case 'ownespionage':
                event_image = image_ownespionage;
                break;
            case 'attack':
                event_image = image_attack;
                break;
            default:
                console.log("Image needed: " + event_typ);
        }

        let direction_image = image_right
        if (event_class.includes("Return"))
        {
            direction_image = image_left
        }
        let direction_append = direction_image;


        let fleettooltip = $("#hidden-div2").children("li").eq(i).children("span").eq(2).children("a").eq(0).attr("data-tooltip-content");
        let fleet_append = '<a href="#" data-tooltip-content="' + fleettooltip + '" class="tooltip ' + event_typ + '"><img src="' + direction_append + '" height="20"></a>';

        let fleettransport = $("#hidden-div2").children("li").eq(i).children("span").eq(2).children("a").eq(3).attr("data-tooltip-content");
        if (fleettransport === undefined)
        {
            fleettransport = $("#hidden-div2").children("li").eq(i).children("span").eq(2).children("a").eq(4).attr("data-tooltip-content");
        }

        let transport_append = '<a href="#" data-tooltip-content="' + fleettransport + '" class="tooltip ' + event_typ + '"><img src="' + event_image + '" height="20"></a>';

        if (fleettransport === undefined)
        {
            transport_append = '<img src="' + event_image + '" height="20">';
        }

        let clock = $("#hidden-div2").children("li").eq(i).children("span").eq(0).text();
        let clock_append = clock

        let countdown = $("#hidden-div2").children("li").eq(i).children("span").eq(1).attr("data-fleet-time");
        let countdown_append = '<div class="timer" style="" data-time="'+ countdown +'">' + timeConvertor(countdown) + '</div>'



        let eventtext = $("#hidden-div2").children("li").eq(i).children("span").eq(2).text();


        let from_cord = $("#hidden-div2").children("li").eq(i).children("span").eq(2).children("a").eq(1).text();



        if (from_cord.includes(":") == false)
        {
            from_cord = $("#hidden-div2").children("li").eq(i).children("span").eq(2).children("a").eq(2).text();

        }

        let from_planetnumber = from_cord.split(":");
        from_planetnumber = Number(from_planetnumber[2].replace(']',''));


        let from_planetname = ""


        if (eventtext.includes("Position"))
        {
            from_planetname = from_cord
        }
        else
        {

            from_planetname = eventtext.split(/Planet |Mond /);
            from_planetname = from_planetname[1].split("]");
            from_planetname = from_planetname[0] + "]";

        }


        let from_link = $("#hidden-div2").children("li").eq(i).children("span").eq(2).children("a").eq(1).attr("href");
        let from_append = '<a href="'+ from_link+'"  style="float:none;" >' + from_planetname + '</a>';


        let to_cord = $("#hidden-div2").children("li").eq(i).children("span").eq(2).children("a").eq(2).text();
        if (to_cord.includes(":") == false)
        {
            to_cord = $("#hidden-div2").children("li").eq(i).children("span").eq(2).children("a").eq(3).text();
        }

        let to_planetnumber = to_cord.split(":");
        to_planetnumber = Number(to_planetnumber[2].replace(']',''));

        let to_planetname = ""
        if (eventtext.includes("Position"))
        {
            to_planetname = to_cord
        }
        else
        {
            to_planetname = eventtext.split(/Planet |Mond /);
        
            if (to_planetname[0].includes("Trümmerfeld "))
            {
                to_planetname = to_planetname[0].split("Trümmerfeld ");
                to_planetname = to_planetname[1].split("]");
                to_planetname = "Trümmerfeld " + to_planetname[0] + "]";
            }
            else if (to_planetname[1].includes("Trümmerfeld "))
            {
                to_planetname = to_planetname[1].split("Trümmerfeld ");
                to_planetname = to_planetname[1].split("]");
                to_planetname = "Trümmerfeld " + to_planetname[0] + "]";
            }
            else
            {
                to_planetname = to_planetname[2].split("]");
                to_planetname = to_planetname[0] + "]";
            }

        }


        let to_link = $("#hidden-div2").children("li").eq(i).children("span").eq(2).children("a").eq(2).attr("href");
        let to_append = '<a href="'+to_link+'"  style="float:none;" >' + to_planetname + '</a>';
        $("content").eq(0).children("div").eq(0).children("div").eq(1).children("table").eq(0).append('<tr><td><span class="' + event_class + '">' + countdown_append + '</span></td><td>' + clock_append + '</td><td>' + transport_append + '</td><td>' + from_append + '</td><td>' + fleet_append + '</td><td>' + to_append + '</td></tr>');

    }

    $("content").eq(0).children("div").eq(0).children("div").eq(1).append('</table><br>');
    $("content").eq(0).children("div").eq(0).children("div").eq(0).text('');
    $("#hidden-div2").remove();
    $("#tn3").remove();
    $( "#chkbtn1" ).remove();


    //Buildorder
    let buildingorder_names = JSON.parse(GM_getValue(uni + currentPlanet + "buildingorder_names", null));
    let buildingorder_timestamps = JSON.parse(GM_getValue(uni + currentPlanet + "buildingorder_timestamps", null));

    $("content").eq(0).children("div").eq(0).children("div").eq(2).children("div").eq(2).append('<table style="width:100%"></table>');
    buildingorder_timestamps.forEach(function (value, i) {
        let name = buildingorder_names[i].split(" ");
        if ((Date.now() / 1000 )< value)
        {
            let countdown = Math.round(value - (Date.now() / 1000));
            $("content").eq(0).children("div").eq(0).children("div").eq(2).children("div").eq(2).children("table").eq(0).append('<tr><td><img src="./styles/theme/nova/gebaeude/' + arrayLookup(name[0],building_images,0,1) + '.gif" width="50" height="50"></td><td>' + buildingorder_names[i] + '</td><td><div class="custom_timer" style="color: #765FBD;" data-time="'+ countdown +'">' + timeConvertor(countdown) + '</div>' + datetimeConvertor(value) + '</td></tr>')
        }
    });


    $("content").eq(0).children("div").eq(0).children("div").eq(0).html('<div class="planeto"><span class="servertime"></span></div>');
}



function RedesignBuilds(building,divid,page,buildings_ingrees_buildid)
{
    let image = $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(building).children("div").eq(1).children("a").eq(0).children("img").eq(0).attr("src");
    let res_need = $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(building).children("div").eq(2).children("span").eq(0).html();
    let energy = $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(building).children("div").eq(1).children("span:last").text();

    let name = $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(building).children("div").eq(0).children("a").eq(0).text();
    let level_string = $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(building).children("div").eq(0).text();
    let level = 0

    if (level_string.includes("("))
    {
        level_string = level_string.split('(');
        level_string = level_string[1].split(' ');
        level_string = level_string[1].replace(')','');
        level = Number(level_string);
    }

    let display = "";
    let info = "";
    res_need = res_need.split('</b>')
    for(let i = 0; i < res_need.length - 1; i++)
    {
        let temp = res_need[i].split('</table>">');
        temp = temp[1].replace("</a>", "");
        temp = temp.replace("</span>", "</span></b>");

        info += temp + '<br>';
    }



    if (page == "buildings")
    {
        if (building == 1 || building == 2 || building == 3)
        {
            info += 'Energie: <b><span class="colorNeutral">' + energy + '</span></b><br>';
        }

        if (building == 5 || building == 7)
        {
            info += 'Energie: <b><span class="colorPositive">' + energy + '</span></b><br>';
        }
    }


    if ($("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(building).hasClass("unavailable"))
    {

        let text = $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(building).children("div").eq(2).children("span").eq(1).text();
        display = '<div style="background-color: rgba(128, 3, 3,0.8); text-align:center; font-size: 12px;">' + text + '</div>';

        info = ""
        let requirements = Number($("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(building).children("div").eq(1).find("a").length);

        for (let i = 1; i < requirements; i++) {
            $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(building).children("div").eq(1).children("a").eq(i).children("span").eq(0).css('white-space','nowrap')
            let temp = $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(building).children("div").eq(1).children("a").eq(i).html();

            info += temp + '<br>';
        }

    } else if ($("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(building).children("div").eq(1).find("div").length > 0)
    {
        if ($("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(building).children("div").eq(2).children("span").eq(1).text() == "Beschäftigt")
        {
            display = '<div style="background-color: rgba(128, 3, 3,0.8); text-align:center; font-size: 12px;">Beschäftigt</div>';
        } else
        {

            let timer = $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(building).children("div").eq(1).children("div").eq(0).children("span").eq(0).attr("timestamp");
            display += '<div class="custom_timer" style="background-color: rgba(255, 214, 0,0.8); text-align:center; color:black; font-size: 12px;" data-time="'+ timer +'">' + timeConvertor(timer) + '</div>'
        }
    } else
    {

        $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(building).children("div").eq(2).children("form").eq(0).children("button").eq(0).removeClass("build_submit");
        let form = '<form action="game.php?page=' + page + '" method="post" class="build_form">'
        form += $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(building).children("div").eq(2).children("form").eq(0).html();
        form += '</form>'
        info += form;
    }

    $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(divid).append('<div style="display: inline-block; vertical-align: top;padding: 5px 5px 5px 5px;margin: 5px 5px 5px 5px; width: 120px; height: 120px; background-image: url(\'' + image + '\');background-size: cover;"><span style="background-color: #036313;font-size: 13px; padding: 2px 2px 2px 2px;">' + level + '</span><br><span style="background-color: #036313; font-size: 10px; display:none;">' + name + '</span><br><span style="background-color: rgba(110, 106, 105,0.7); font-size: 9px;">' + display + '</span><span style="background-color: rgba(110, 106, 105,0.9); font-size: 9px; display:none;">' + info + '</span></div>')
    $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(divid).children("div:last-child").on( "mouseenter", function() {
        $(this).children("span").eq(1).show();
        $(this).children("span").eq(3).show();
    } ).on("mouseleave", function() {
        $(this).children("span").eq(1).hide();
        $(this).children("span").eq(3).hide();
    } );


}

function Buildings() {

    var lenght = Number($("#buildlist > div").length);

    const buildingorder_names = [];
    const buildingorder_timestamps = [];
    for (let i = 0; i < lenght; i++) {
        if (isEven(i))
        {
            if (lenght === 10)
            {
                let name = $("#buildlist").children("div").eq(i).text().split(":");
                buildingorder_names.push(name[1].trim());
            }
            else
            {
                buildingorder_names.push($("#buildlist").children("div").eq(i).children("form").eq(0).children("button").eq(0).text());
            }
        }
        else
        {
            buildingorder_timestamps.push(Number($("#buildlist").children("div").eq(i).children("span").attr("data-time")));

        }
    }
    GM_setValue(uni + currentPlanet + "buildingorder_names", JSON.stringify(buildingorder_names));
    GM_setValue(uni + currentPlanet + "buildingorder_timestamps", JSON.stringify(buildingorder_timestamps));



    $('.buildn').css("background-color","#161618");
    $('.buildn').css("background-color","#161618");


    var buildings_ingrees_buildid = 0
    if ($("#buildlist")[0]){
        buildings_ingrees_buildid = 1
    }

    $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(0).remove();

    $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(0).before('<div class="infos" style="text-align: left"></div>');
    $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(0).append('<div class="planeto">Minen</div>')

    RedesignBuilds(1,0,"buildings",buildings_ingrees_buildid)
    RedesignBuilds(2,0,"buildings",buildings_ingrees_buildid)
    RedesignBuilds(3,0,"buildings",buildings_ingrees_buildid)

    $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(0).after('<div class="infos" style="text-align: left"></div>');
    $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(1).append('<div class="planeto">Energie</div>')

    RedesignBuilds(5,1,"buildings",buildings_ingrees_buildid)
    RedesignBuilds(7,1,"buildings",buildings_ingrees_buildid)

    $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(1).after('<div class="infos" style="text-align: left"></div>');
    $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(2).append('<div class="planeto">Speicher</div>')

    RedesignBuilds(12,2,"buildings",buildings_ingrees_buildid)
    RedesignBuilds(13,2,"buildings",buildings_ingrees_buildid)
    RedesignBuilds(14,2,"buildings",buildings_ingrees_buildid)

    $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(2).after('<div class="infos" style="text-align: left"></div>');
    $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(3).append('<div class="planeto">Baugeschwindigkeit</div>')

    RedesignBuilds(10,3,"buildings",buildings_ingrees_buildid)
    RedesignBuilds(11,3,"buildings",buildings_ingrees_buildid)

    $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(3).after('<div class="infos" style="text-align: left"></div>');
    $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(4).append('<div class="planeto">Produktion</div>')

    RedesignBuilds(13,4,"buildings",buildings_ingrees_buildid)

    if (uni == "uni4")
    {
        RedesignBuilds(21,4,"buildings",buildings_ingrees_buildid)
    } else
    {
        RedesignBuilds(20,4,"buildings",buildings_ingrees_buildid)
    }

    $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(4).after('<div class="infos" style="text-align: left"></div>');
    $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(5).append('<div class="planeto">Forschung</div>')

    RedesignBuilds(18,5,"buildings",buildings_ingrees_buildid)
    RedesignBuilds(10,5,"buildings",buildings_ingrees_buildid)

    $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(5).after('<div class="infos" style="text-align: left"></div>');
    $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(6).append('<div class="planeto">Sonstiges</div>')

    RedesignBuilds(20,6,"buildings",buildings_ingrees_buildid)
    RedesignBuilds(21,6,"buildings",buildings_ingrees_buildid)


    let remotecount = 16

    if (uni == "uni4")
    {
        RedesignBuilds(22,6,"buildings",buildings_ingrees_buildid);
        remotecount = 17;
    }



    for (let i = 1; i <= remotecount; i++) {
        $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(7).remove();
    }
}


function Research() {
    var buildings_ingrees_buildid = 0
    if ($("#buildlist")[0]){
        buildings_ingrees_buildid = 1
    }

    $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(0).remove();

    $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(0).before('<div class="infos" style="text-align: left"></div>');
    $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(0).append('<div class="planeto">Flotte</div>')

    RedesignBuilds(8,0,"research",buildings_ingrees_buildid)
    RedesignBuilds(9,0,"research",buildings_ingrees_buildid)
    RedesignBuilds(10,0,"research",buildings_ingrees_buildid)
    RedesignBuilds(2,0,"research",buildings_ingrees_buildid)

    $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(0).before('<div class="infos" style="text-align: left"></div>');
    $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(1).append('<div class="planeto">Kampf</div>')

    RedesignBuilds(6,1,"research",buildings_ingrees_buildid)
    RedesignBuilds(4,1,"research",buildings_ingrees_buildid)
    RedesignBuilds(5,1,"research",buildings_ingrees_buildid)
    RedesignBuilds(2,1,"research",buildings_ingrees_buildid)
    RedesignBuilds(20,1,"research",buildings_ingrees_buildid)

    $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(1).before('<div class="infos" style="text-align: left"></div>');
    $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(2).append('<div class="planeto">Produktion</div>')

    RedesignBuilds(18,2,"research",buildings_ingrees_buildid)
    RedesignBuilds(19,2,"research",buildings_ingrees_buildid)
    RedesignBuilds(20,2,"research",buildings_ingrees_buildid)

    $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(2).before('<div class="infos" style="text-align: left"></div>');
    $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(3).append('<div class="planeto">Expansion</div>')

    RedesignBuilds(18,3,"research",buildings_ingrees_buildid)
    RedesignBuilds(17,3,"research",buildings_ingrees_buildid)

    $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(3).before('<div class="infos" style="text-align: left"></div>');
    $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(4).append('<div class="planeto">Grundlagen</div>')

    RedesignBuilds(10,4,"research",buildings_ingrees_buildid)
    RedesignBuilds(11,4,"research",buildings_ingrees_buildid)
    RedesignBuilds(15,4,"research",buildings_ingrees_buildid)
    RedesignBuilds(16,4,"research",buildings_ingrees_buildid)
    RedesignBuilds(17,4,"research",buildings_ingrees_buildid)


    for (let i = 1; i <=19; i++) {
        $("content").eq(0).children("div").eq(0 + buildings_ingrees_buildid).children("div").eq(5).remove();
    }
}


function Shipyard() {
    $('.buildn').css("background-color","#161618");
    $('.buildn').css("background-color","#161618");
}


function Defence() {
    $('.buildn').css("background-color","#161618");
    $('.buildn').css("background-color","#161618");
}




function Settings(){
    $("content").eq(0).children("table").eq(0).children("tbody").eq(0).children("tr").eq(31).after('<tr><th colspan="2">pr0game Advanced Einstellungen</th><tr>');
    $("content").eq(0).children("table").eq(0).children("tbody").eq(0).children("tr").eq(32).after('<tr><td>Gebäudeansicht anpassen</td><td><input type="checkbox" value="1" checked="checked"></td></tr>');
    $("content").eq(0).children("table").eq(0).children("tbody").eq(0).children("tr").eq(33).children("td").eq(1).children("input").eq(0).change(function() {
        if(this.checked) {
            GM_setValue(uni + "showbuilding", 1);
        } else
        {
            GM_setValue(uni + "showbuilding", 0);
        }
    });

    if (GM_getValue(uni + "showbuilding", 1) == 1)
    {
        $("content").eq(0).children("table").eq(0).children("tbody").eq(0).children("tr").eq(33).children("td").eq(1).children("input").eq(0).prop( "checked", true );
    }
    else
    {
        $("content").eq(0).children("table").eq(0).children("tbody").eq(0).children("tr").eq(33).children("td").eq(1).children("input").eq(0).prop( "checked", false );
    }




    $("content").eq(0).children("table").eq(0).children("tbody").eq(0).children("tr").eq(33).after('<tr><td>Forschungsansicht anpassen</td><td><input type="checkbox" value="1" checked="checked"></td></tr>');
    $("content").eq(0).children("table").eq(0).children("tbody").eq(0).children("tr").eq(34).children("td").eq(1).children("input").eq(0).change(function() {
        if(this.checked) {
            GM_setValue(uni + "showresearch", 1);
        } else
        {
            GM_setValue(uni + "showresearch", 0);
        }
    });

    if (GM_getValue(uni + "showresearch", 1) == 1)
    {
        $("content").eq(0).children("table").eq(0).children("tbody").eq(0).children("tr").eq(34).children("td").eq(1).children("input").eq(0).prop( "checked", true );
    }
    else
    {
        $("content").eq(0).children("table").eq(0).children("tbody").eq(0).children("tr").eq(34).children("td").eq(1).children("input").eq(0).prop( "checked", false );
    }
}


(function() {
    'use strict';
    //Player-Id
    if (GM_getValue(uni + "playerid", null) == null)
    {
        GetPlayerId();
    }

    if (urlPage == 'Empire') {
        Imperium()
    }
    HeaderProduction();
    Menu();


    if (urlPage == 'overview') {
        Overview()
    }

    if (urlPage == 'research') {
        if (GM_getValue(uni + "showresearch", 1) == 1)
        {
        Research()
        }
    }

    if (urlPage == 'buildings') {

        if (GM_getValue(uni + "showbuilding", 1) == 1)
        {
            Buildings()
        }
    }

    if (urlPage == 'shipyard' && $("content").eq(0).find("div.planeto").length > 0) {
        Shipyard()
    }

    if (urlPage == 'shipyard' && $("content").eq(0).find("div.planeto").length == 0) {
        Defence()
    }

    if (urlPage == 'galaxy') {
        GM_addStyle("a:visited { color: unset !important  }");
    }

    if (urlPage == 'settings') {
        Settings()
    }

    let timer = self.setInterval(TimerUpdate, 1000);
})();