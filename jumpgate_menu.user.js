// ==UserScript==
// @name         pr0game jumpgate menu
// @namespace    https://pr0game.pebkac.me/
// @version      1.0
// @description  Add direct jumpgate button to main menu if moon is selected. Does not check if jumpgate is built.
// @author       d0xxy
// @match        https://pr0game.com/*/game.php*
// @updateURL    https://codeberg.org/pr0game/userscripts/raw/branch/master/jumpgate_menu.user.js
// @downloadURL  https://codeberg.org/pr0game/userscripts/raw/branch/master/jumpgate_menu.user.js
// @supportURL   https://codeberg.org/pr0game/userscripts/issues
// @icon         https://pr0game.com/favicon.ico
// ==/UserScript==

(function() {
    'use strict';

    function addMenuButton (text, href, pos) {
        // add button to menu
        const listEntry = document.createElement('li')
        const listLink = document.createElement('a')
        listLink.href = href
        listLink.text = text
        listEntry.appendChild(listLink)
        const ref = document.getElementById('menu').children[pos]
        return ref.insertAdjacentElement('afterend', listEntry)
    }

    function isMoon () {
        return document.querySelector('.planetSelectorWrapper > a > img').src.includes('/mond.jpg')
    }

    if (isMoon()) {
        const elm = addMenuButton ('Jumpgate', '#', 8)
        elm.querySelector('a').setAttribute('onclick', 'return Dialog.info(43)')
    }

})();
