// ==UserScript==
// @name         Nerdy Names
// @namespace    http://tampermonkey.net/
// @version      1.1
// @description  Exchange "normal" ship, missile, tech and defense names with short nerdy ones.
// @author       Bananenfuerst
// @match        https://pr0game.com/*/game.php?page=messages*
// @match        https://pr0game.com/*/game.php?page=overview*
// @match        https://pr0game.com/*/game.php?page=fleetTable*
// @match        https://pr0game.com/*/game.php?page=imperium*
// @match        https://pr0game.com/*/game.php?page=techtree*
// @match        https://pr0game.com/*/game.php?page=raport*
// @match        https://pr0game.com/*/game.php?page=phalanx*
// @icon         https://pr0game.com/favicon.ico
// @grant        none
// ==/UserScript==

(function() {

    'use strict';
    const doc = $(document);
    const baddne = document.createElement('button');
    let elements = [];
    let popups = [];
    let nerdified = false;
    let origPage, nerdyPage;

    const replaceDict = {
        KT: ["Kleiner Transporter","Kl. Transporter","Kleine Transporter","Kleines Handelsschiff","Kl. Handelsschiff","Light Cargo"],
        GT: ["Gro�er Transporter","Gr. Transporter","Gro�e Transporter","Gro�es Handelsschiff","Gr. Handelsschiff","Heavy Cargo"],
        LJ: ["Leichter J�ger","L. J�ger","Leichte Brig","L. Brig","Light Fighter"],
        SJ: ["Schwerer J�ger","S. J�ger","Schwere Brig","S. Brig","Heavy Fighter"],
        Xer: ["Kreuzer","Schoner","Cruiser"],
        BB: ["Planet Bomber","Bomber","Kanonenschiff"], // Reihenfolge gedreht weil Fehler mit Englisch
        SS: ["Schlachtschiff","Korvette","Battleship"],
        SXer: ["Schlachtkreuzer","Fregatte","Battle Cruiser", "Battle Xer"], // lol Dummer fix
        Zerri: ["Zerst�rer","Linienschiff","Star Fighter","Destroyer"],
        RIP: ["Todesstern","Schwimmende Festung","Battle Fortress","Death Star"],
        Rec: ["Recycler","Wracktaucher","Recycler"],
        Kolo: ["Kolonieschiff","Pilgerschiff","Colony Ship"],
        Spio: ["Spionagesonde","Sp�hpapagei","Spy Probe"],
        Sat: ["Solarsatellit","Fischerboot","Solar Satellite"],
        Rak: ["Raketenwerfer","12-Pf�nder","Missile Launcher"],
        LL: ["Leichtes Lasergesch�tz","Balliste","Light Laser Turret"],
        SL: ["Schweres Lasergesch�tz","Feuerballiste","Heavy Laser Turret"],
        Gau�: ["Gau�kanone","30-Pf�nder","Gauss Cannon"],
        Ionen: ["Ionengesch�tz","Kart�tsche","Ion Cannon"],
        Plasi: ["Plasmawerfer","M�rser","Plasma Cannon"],
        KS: ["Kleine Schildkuppel","Holzwall","Small Shield Dome"],
        GS: ["Gro�e Schildkuppel","Steinwall","Large Shield Dome"],
        ARAK: ["Abfangrakete","L�schvorrichtung","Interceptor"],
        IRAK: ["Interplanetarrakete","Brandbombe","Interplanetary missiles"],
        Waffen: ["Waffentechnik","Kanonentechnik","Weapons Technology"],
        Schilde: ["Schildtechnik","Holzbeschaffenheit","Shield Technology"],
        Panzer: ["Raumschiffpanzerung","Schiffsbauwesen","Armour Technology"],
        Verbrenner: ["Verbrennungstriebwerk","Ruderwerk","Combustion Engine"],
        Impuls: ["Impulstriebwerk","Segelkunde","Impulse Engine"],
        Hyperraum: ["Hyperraumantrieb","Stromlinienforschung","Hyperspace Engine"],
        Met: ["Metall","Holz","Metal"],
        Kris: ["Kristall","Gold","Crystal"],
        Deut: ["Deuterium","Rum","Deuterium"],
        Plani: ["Planet","Hafen","Planet"],
        Mond: ["Mond","Insel","Moon"],
    };

    const nerdify = function() {
        let n, walk=document.createTreeWalker(document.body, NodeFilter.SHOW_TEXT, null);
        while (n=walk.nextNode()) elements.push([n, n.nodeValue]);

        n = undefined;
        walk = document.createTreeWalker(document.body, NodeFilter.SHOW_ALL, (node) =>
            node?.dataset?.tooltipContent ? NodeFilter.FILTER_ACCEPT : NodeFilter.FILTER_SKIP
        );
        while (n=walk.nextNode()) popups.push([n, n.dataset.tooltipContent]);

        // "translate" visible text
        for (const entry of elements) {
            let nerdyValue = entry[1];
            for (const nerdy in replaceDict) {
                for (const normal of replaceDict[nerdy]) {
                    nerdyValue = nerdyValue.replace(new RegExp(normal,'g'), nerdy);
                }
            }
            entry.push(nerdyValue);
        }

        // "translate" popups
        for (const entry of popups) {
            let nerdyValue = entry[1];
            for (const nerdy in replaceDict) {
                for (const normal of replaceDict[nerdy]) {
                    nerdyValue = nerdyValue.replace(new RegExp(normal,'g'), nerdy);
                }
            }
            entry.push(nerdyValue);
        }
    };

    const toggleNerd = function(event) {
        const takeIndex = nerdified ? 1 : 2;

        // replace content for visible text
        for (const entry of elements) {
            entry[0].nodeValue = entry[takeIndex];
        }

        // replace Pop-up contents
        for (const entry of popups) {
            entry[0].dataset.tooltipContent = entry[takeIndex];
        }
        nerdified = !nerdified;
        baddne.innerText = nerdified ? 'Normale Namen' : 'Kurze Namen';
    };


    doc.ready(function() {
        baddne.type = "button";
        baddne.innerHTML = 'Kurze Namen';
        baddne.id = "btn-nerdy-names-switcher";
        baddne.addEventListener('click', toggleNerd);
        baddne.style.marginLeft = '1em';
        baddne.style.marginTop = '0px';
        baddne.style.marginBottom = '0px';
        baddne.style.verticalAlign = 'baseline';

        // place baddne depending on current page
        const href = window.location.href;
        if (/page=overview*/.test(href)) {
            baddne.style.marginLeft = 'unset';
            $(baddne).appendTo($('#chkbtn1').parent());
        } else if (/page=fleetTable*/.test(href)) {
            $(baddne).appendTo($('form>table tr:first-child th:first-child'));
        } else if (/page=messages*/.test(href)) {
            $(baddne).appendTo($('content>table:first-child tr:first-child th:first-child'));
        } else if (/page=imperium*/.test(href)) {
            $(baddne).appendTo($('table:first-child tr:first-child th:first-child'));
        } else if (/page=techtree*/.test(href)) {
            baddne.style.marginLeft = 'unset';
            $(baddne).prependTo($('content'));
        } else if (/page=raport*/.test(href)) {
            $(baddne).prependTo($('table').first());
        } else if (/page=phalanx*/.test(href)) {
            $(baddne).appendTo($('table[width="90%"] tr:nth-child(2) th[colspan="3"]'));
        }

        nerdify();
        const autoModeSetting = localStorage.getItem('Bananenfuerst.NerdyNames.asLanguage');
        if (autoModeSetting === "true") {
            toggleNerd();
        }
    });
})();