// ==UserScript==
// @name        allmessages
// @version     1.0.0
// @author      d0xxy
// @description Opens the 'all messages' tab by default when clicking messages from the side menu
// @match       pr0game.com/*/game.php*
// @downloadURL https://codeberg.org/pr0game/userscripts/raw/branch/master/allmessages.user.js
// @updateURL   https://codeberg.org/pr0game/userscripts/raw/branch/master/allmessages.user.js
// ==/UserScript==

(()=>{"use strict";!function(){const e=e=>e.setAttribute("href","game.php?page=messages&category=100"),s=document.querySelector("div.message a");s&&e(s),e(Array.from(document.querySelectorAll("ul#menu a")).find((e=>e.href.includes("page=messages"))))}()})();