// ==UserScript==
// @name         pr0game Scavengers Toolbox
// @namespace    http://pr0game.pebkac.me
// @version      10.0
// @description  Improvements mainly for scavenging inactive players
// @author       AxelFLOSS, Timo_Ka
// @match        https://pr0game.com/*/game.php?*page=messages*
// @match        https://pr0game.com/*/game.php?*page=fleetTable*
// @match        https://pr0game.com/*/game.php?*page=research*
// @match        https://pr0game.com/*/game.php?*page=marketPlace
// @updateURL    https://codeberg.org/pr0game/userscripts/raw/branch/master/pr0game_scavengers_toolbox.user.js
// @downloadURL  https://codeberg.org/pr0game/userscripts/raw/branch/master/pr0game_scavengers_toolbox.user.js
// @supportURL   https://codeberg.org/pr0game/userscripts/issues
// @icon         https://pr0game.com/favicon.ico
// @grant        GM_addStyle
// @grant        GM.setValue
// @grant        GM.getValue
// ==/UserScript==
//Not needed anymore!