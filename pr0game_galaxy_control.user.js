// ==UserScript==
// @name         pr0game Galaxy Control
// @namespace    https://pr0game.pebkac.me/
// @version      10.0
// @description  Control the Galaxy view with your arrow keys.
// @author       AxelFLOSS
// @match        https://pr0game.com/*/game.php?*page=galaxy*
// @updateURL    https://codeberg.org/pr0game/userscripts/raw/branch/master/pr0game_galaxy_control.user.js
// @downloadURL  https://codeberg.org/pr0game/userscripts/raw/branch/master/pr0game_galaxy_control.user.js
// @supportURL   https://codeberg.org/pr0game/userscripts/issues
// @icon         https://pr0game.com/favicon.ico
// @grant        none
// ==/UserScript==
//Not needed anymore!

