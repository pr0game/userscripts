// ==UserScript==
// @name         Kampfbericht Anonymizer
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://pr0game.com/game.php?page=raport&raport=*
// @match        https://pr0game.com/*/game.php?page=raport&raport=*
// @match        https://www.pr0game.com/game.php?page=raport&raport=*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=pr0game.com
// @grant        unsafeWindow
// ==/UserScript==

(function() {
    'use strict';

    window.anonymizeBattleReport = function() {
        $("head").append('<style>.anonymize { filter: blur(5px); }</style>');
        $("body").html($("body").html().replace(/\[([0-9]{1,3})\:([0-9]{1,3})\:([0-9]{1,3})\]/g,'[<span class="anonymize">$1:$2:$3</span>]'));

        var cells = $('td');
        cells.each(function(key, obj) {
            $(obj).html($(obj).html().replace(/([0-9]{3,})\%/g,' <span class="anonymize">$1%</span>'));
        });

        var rounds = $("body").html().split('<hr>');
        var roundCount = rounds.length;
        var returnHtml = '';
        for(var i = 0; i < roundCount; i++) {
            if(i === 0) {
                returnHtml += rounds[i];
            }

            if(i === roundCount - 1) {
                returnHtml += rounds[i];
            }
        }

        $("body").html(returnHtml);
    };

    $('body').prepend('<button id="anonymize">Anonymisieren</button>');
    $('#anonymize').click(function() {
        window.anonymizeBattleReport();
        $('#anonymize').remove();
    });
})();