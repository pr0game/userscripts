'use strict';
class RaidParser {

  constructor() {
  
  }
  isRaidPage() {
    return window.location.search.includes("page=messages") && window.location.search.includes("category=3")
  }
  isRecPage() {
    return window.location.search.includes("page=messages") && window.location.search.includes("category=5")
  }
  getRaidMessages() {
    const htmlElements = document.getElementsByClassName("message_head")
    const messages = []
    for (const m of htmlElements) {
      if (!m) return
      const id = parseInt(m.id.split("_")[1])
      const header = m.innerText.trim()
      const body = document.getElementsByClassName(`message_${id} messages_body`)[0].innerText.trim()
      const parent = document.getElementsByClassName(`message_${id} messages_body`)[0]
      const Metaltype = parent.getElementsByClassName('reportSteal element901')[0]
      let Metal = 0
      let Crystal = 0
      let Deuterium = 0
      if (typeof Metaltype !== 'undefined'){
        Metal = parseInt(parent.getElementsByClassName('reportSteal element901')[0].innerText.replace(/(\d)[\s.]+(?=\d)/g, '$1'))
        Crystal = parseInt(parent.getElementsByClassName('reportSteal element902')[0].innerText.replace(/(\d)[\s.]+(?=\d)/g, '$1'))
        Deuterium = parseInt(parent.getElementsByClassName('reportSteal element903')[0].innerText.replace(/(\d)[\s.]+(?=\d)/g, '$1'))
      }

      
      messages.push({
        id,
        header,
        body,
        Metal,
        Crystal,
        Deuterium
      })
    }
    return messages
  }
  getRecMessages() {
    const htmlElements = document.getElementsByClassName("message_head")
    const messages = []
    for (const m of htmlElements) {
      if (!m) return
      if (recMap.includes(m.cells[3].innerText.trim())){
        const id = parseInt(m.id.split("_")[1])
        const header = m.innerText.trim()
        const body = document.getElementsByClassName(`message_${id} messages_body`)[0].innerText.trim()
        const parent = document.getElementsByClassName(`message_${id} messages_body`)[0]
        let mtch = parent.innerText.replace(/(\[\d:\d{1,3}:\d{1,2}\])/i, "")
        mtch = mtch.replaceAll(".","").match(/\d+/g);
        let Metal = 0
        let Crystal = 0
        if (typeof mtch !== 'undefined'){
          Metal = parseInt(mtch[0])
          Crystal = parseInt(mtch[1])
        }

        
        messages.push({
          id,
          header,
          body,
          Metal,
          Crystal
        })
      }
    }
    return messages
  }
  parse_text({
    id,
    header,
    body,
    Metal,
    Crystal,
    Deuterium
  }) {
    function parseDate(dateRaw) {
      const monthMapping = languageMap.months
      // make sure date format is english
      for (let monthDE in monthMapping) {
        dateRaw = dateRaw.replace(monthDE, monthMapping[monthDE])
      }
      // move around day so it can be parsed
      const parts = dateRaw.split(" ")
      const date = new Date([parts[1], parts[0].replace(".", ""), parts.slice(2)].join(" "))
      return date
    }
    function formatDate(date) {
      // Get timezone offset in ms
      var offset = (new Date()).getTimezoneOffset() * 60000;
      
      // Get iso time and slice timezone
      var dateWithoutTimeZone = (new Date(new Date(date) - offset)).toISOString().slice(0,-1);
      
      var d = new Date(dateWithoutTimeZone);
      
      var year = d.getFullYear();
      var month = d.getMonth() + 1;
      var day = d.getDate();
      var hour = d.getHours();
      var minute = d.getMinutes();
      var second = d.getSeconds();
      
      return `${year}-${(month > 9 ? '' : '0') + month}-${(day > 9 ? '' : '0') + day}T${(hour > 9 ? '' : '0') + hour}:${(minute > 9 ? '' : '0') + minute}:${(second > 9 ? '' : '0') + second}`;
      }

    function parsePlanet(report) {
      return report.split("[")[1].split("]")[0]
    }

    const [dateRaw, ] = header.split(/\t/)
    const preDate = parseDate(dateRaw)
    const date = formatDate(preDate).toString()
    const planet = parsePlanet(body)
    return {
      id,
      date,
      planet,
      Metal,
      Crystal,
      Deuterium
    }
  }
}

const languageMap = {
  months: {
    "Jan": "Jan",
    "Feb": "Feb",
    "Mär": "Mar",
    "Apr": "Apr",
    "Mai": "May",
    "Jun": "Jun",
    "Jul": "Jul",
    "Aug": "Aug",
    "Sep": "Sep",
    "Okt": "Oct",
    "Nov": "Nov",
    "Dez": "Dec"
  },
}

const recMap = [
    "Recycling Report",
    "Recyclerbericht",
    "Reporte de reciclaje",
    "Rapport de recyclage",
    "Rescheiklerberichd",
    "Wrackbericht",
    "Raport zbierania",
    "Relatório de reciclagem",
    "Доклад переработчиков",
    "Geri Donusum Raporu"
]

module.exports = {
  RaidParser,
  languageMap,
  recMap,
}