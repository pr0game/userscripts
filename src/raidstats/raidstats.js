'use strict';

const {
  RaidParser
} = require('./raidParser')
const Chart = window['Chart']
let chartObj
const ep = new RaidParser()

function updateRaidDB() {
  const raids = ep.getRaidMessages().map(m => ep.parse_text(m))
  const raidDB = GM_getValue("raids", [])
  const existingRaidsIds = raidDB.map(e => e.id + e.date.split("T")[0]) // match by id AND date in case game gets reset and ids are starting from 0 again
  let added = 0
  for (const raid of raids) {
    if (existingRaidsIds.indexOf(raid.id + raid.date.split("T")[0]) < 0) {
      raidDB.push(raid)
      added += 1
    }
  }
  GM_setValue("raids", raidDB)
  console.log(`Added ${added} new raids to database, now contains ${raidDB.length} entries`)
}
function updateRecDB() {
  const recs = ep.getRecMessages().map(m => ep.parse_text(m))
  const recDB = GM_getValue("recs", [])
  const existingRecsIds = recDB.map(e => e.id + e.date.split("T")[0]) // match by id AND date in case game gets reset and ids are starting from 0 again
  let added = 0
  for (const rec of recs) {
    if (existingRecsIds.indexOf(rec.id + rec.date.split("T")[0]) < 0) {
      recDB.push(rec)
      added += 1
    }
  }
  GM_setValue("recs", recDB)
  console.log(`Added ${added} new recs to database, now contains ${recDB.length} entries`)
}

function createChart(type = "bar") {
  const backgroundPlugin = {
    id: 'custom_canvas_background_color',
    beforeDraw: (chart) => {
      const ctx = chart.canvas.getContext('2d')
      ctx.save()
      ctx.globalCompositeOperation = 'destination-over'
      ctx.fillStyle = "rgba(250, 250, 250, 0.9)"
      ctx.fillRect(0, 0, chart.width, chart.height)
      ctx.restore()
    }
  }
  const ctx = document.getElementById('charts').getContext('2d')
  //todo: darkmode
  chartObj = new Chart(ctx, {
    type,
    data: {},
    options: {
      scales: {
        x: {
          stacked: true
        },
        y: {
          stacked: true,
          beginAtZero: true
        }
      },
      responsive: true,
      maintainAspectRatio: false
    },
    plugins: [
      backgroundPlugin
    ]
  })
  return chartObj
}

function GM_addStyle(css) {
  const style = document.getElementById("GM_addStyleBy8626") || (function () {
    const style = document.createElement('style');
    style.type = 'text/css';
    style.id = "GM_addStyleBy8626";
    document.head.appendChild(style);
    return style;
  })();
  const sheet = style.sheet;
  sheet.insertRule(css, (sheet.rules || sheet.cssRules || []).length);
}

function addChart() {
  GM_addStyle(".raidstats-canvas { width: 100%; height: 400px; margin: 3px; }");
  const d = document.createElement("div")
  const c = document.createElement("canvas")
  d.appendChild(c)
  d.classList = "raidstats-canvas"
  c.id = "charts"
  const content = document.getElementsByTagName("content")[0]
  content.insertAdjacentElement("beforeend", d)

  function createOption(value, text) {
    const e = document.createElement("option")
    e.value = value
    e.text = text
    return e
  }
  const table = document.createElement("table")
  const row1 = document.createElement("tr")
  const row2 = document.createElement("tr")
  const td1 = document.createElement("td")
  const td2 = document.createElement("td")
  const td3 = document.createElement("td")
  const td4 = document.createElement("td")

  const td5 = document.createElement("td")
  const td6 = document.createElement("td")
  const td7 = document.createElement("td")
  const td8 = document.createElement("td")
  const s = document.createElement("select")
  s.id = "chartSelect"
  s.addEventListener("change", (event) => draw(event.target.value))
  s.appendChild(createOption("dailyRes", "Daily Resources"))
  s.appendChild(createOption("dailyRec", "Daily Recycler"))
  s.appendChild(createOption("dailyAll", "Daily All"))
  td1.insertAdjacentElement("afterbegin", s)
  //d.insertAdjacentElement("beforebegin", s)

  let checkvalue = GM_getValue("log")
  if (checkvalue != 1 && checkvalue != 0) {
    checkvalue = 0
  }
  const check = document.createElement("input")
  check.type = "checkbox"
  check.id = "logarithmic"
  check.checked = checkvalue
  check.addEventListener("change", (event) => save("log", check.checked))
  //s.insertAdjacentElement("afterend", check)
  td6.insertAdjacentElement("afterbegin", check)

  const checkText = document.createElement("text")
  checkText.innerText = "Logarithmic"
  td2.insertAdjacentElement("afterbegin", checkText)
  //check.insertAdjacentElement("afterend", checkText)

  let start = GM_getValue("startDate")
  if (typeof start === 'undefined') {
    start = "2021-12-24"
  }
  const startDate = document.createElement("input")
  startDate.value = start
  startDate.type = "date"
  startDate.min = "2021-12-24"
  startDate.max = "2032-12-31"
  startDate.style = "color-scheme: dark;"
  startDate.addEventListener("change", (event) => save("startDate", startDate.value))
  //checkText.insertAdjacentElement("afterend", startDate)
  td7.insertAdjacentElement("afterbegin", startDate)

  const startText = document.createElement("text")
  startText.innerText = "Start Date"
  //startDate.insertAdjacentElement("afterend", startText)
  td3.insertAdjacentElement("afterbegin", startText)

  let end = GM_getValue("endDate")
  if (typeof end === 'undefined') {
    end = "2032-12-31"
  }
  const endDate = document.createElement("input")
  endDate.value = end
  endDate.type = "date"
  endDate.min = "2021-12-24"
  endDate.max = "2032-12-31"
  endDate.style = "color-scheme: dark;"
  endDate.addEventListener("change", (event) => save("endDate", endDate.value))
  //startText.insertAdjacentElement("afterend", endDate)
  td8.insertAdjacentElement("afterbegin", endDate)

  const endText = document.createElement("text")
  endText.innerText = "End Date"
  //endDate.insertAdjacentElement("afterend", endText)
  td4.insertAdjacentElement("afterbegin", endText)
  row1.appendChild(td1)
  row1.appendChild(td2)
  row1.appendChild(td3)
  row1.appendChild(td4)
  row2.appendChild(td5)
  row2.appendChild(td6)
  row2.appendChild(td7)
  row2.appendChild(td8)
  table.appendChild(row1)
  table.appendChild(row2)
  d.insertAdjacentElement("beforebegin", table)
  // create chart object
  chartObj = createChart("bar")

}

function save(name, value) {
  GM_setValue(name, value)
}

function filterDates(start, end, dates) {
  let newDates = []
  for (const day of dates) {
    if (day >= start && day <= end) {
      newDates.push(day)
    }
  }
  return newDates
}
function getChartDataDailyRes() {
  const raidDB = GM_getValue("raids")
  const dates = raidDB.map(e => e.date.split("T")[0])

  let uniqueDates = [...new Set(dates)].sort()
  uniqueDates = filterDates(GM_getValue("startDate"), GM_getValue("endDate"), uniqueDates)
  const resTypes = ["Metal", "Crystal", "Deuterium"]
  const datasets = []
  const raids = raidDB
  for (const resType of resTypes) {
    const data = []
    for (const date of uniqueDates) {
      let sum = 0
      for (const raid of raids.filter(e => e.date.split("T")[0] == date)){
        //console.log(raid[resType])
        sum += raid[resType]
        //console.log("raid: ", raid)
        //console.log("restype: ", resType)
      }
      data.push(sum)
    }
    let color = "#000"
    switch (resType) {
      case "Metal":
        color = "#666"
        break
      case "Crystal":
        color = "#d3d"
        break
      case "Deuterium":
        color = "#0d7"
    }
    datasets.push({
      label: resType,
      data,
      backgroundColor: [color]
    })
  }

  let labels = uniqueDates
  const data = {
    labels,
    datasets
  }
  return data
}

function getChartDataDailyRec() {
  const recDB = GM_getValue("recs")
  const dates = recDB.map(e => e.date.split("T")[0])

  let uniqueDates = [...new Set(dates)].sort()
  uniqueDates = filterDates(GM_getValue("startDate"), GM_getValue("endDate"), uniqueDates)
  const resTypes = ["Metal", "Crystal"]
  const datasets = []
  const recs = recDB
  for (const resType of resTypes) {
    const data = []
    for (const date of uniqueDates) {
      let sum = 0
      for (const rec of recs.filter(e => e.date.split("T")[0] == date)){
        //console.log(recs[resType])
        sum += rec[resType]
        //console.log("raid: ", raid)
        //console.log("restype: ", resType)
      }
      data.push(sum)
    }
    let color = "#000"
    switch (resType) {
      case "Metal":
        color = "#666"
        break
      case "Crystal":
        color = "#d3d"
    }
    datasets.push({
      label: resType,
      data,
      backgroundColor: [color]
    })
  }

let labels = uniqueDates
const data = {
  labels,
  datasets
}
return data
}

function getChartDataDailyAll() {
  const recDB = GM_getValue("recs")
  const raidDB = GM_getValue("raids")
  const recDates = recDB.map(e => e.date.split("T")[0])
  const raidDates = raidDB.map(e => e.date.split("T")[0])

  const uniqueRecDates = [...new Set(recDates)].sort()
  let uniqueRaidDates = [...new Set(raidDates)].sort()
  for (const day of uniqueRecDates) {
    uniqueRaidDates.push(day)
  }
  let uniqueDates = [...new Set(uniqueRaidDates)].sort()
  uniqueDates = filterDates(GM_getValue("startDate"), GM_getValue("endDate"), uniqueDates)
  const resTypes = ["Metal", "Crystal", "Deuterium"]
  const Types = ["Raid", "Rec"]
  const datasets = []
  const recs = recDB
  const raids = raidDB
  for (const resType of resTypes) {
    for (const type of Types) {
      const data = []
      for (const date of uniqueDates) {
        let sum = 0
        if(type == "Raid") {
          for (const raid of raids.filter(e => e.date.split("T")[0] == date)){
            sum += raid[resType]
          }
        } else if (type == "Rec") {
          for (const rec of recs.filter(e => e.date.split("T")[0] == date)){
            sum += rec[resType]
          }
        }
        
        data.push(sum)
      }
      let color = "#000"
      if (resType == "Metal" && type =="Raid") {
        color = "#666"
      } else if (resType == "Crystal" && type =="Raid") {
        color = "#d3d"
      } else if (resType == "Deuterium" && type =="Raid") {
        color = "#0d7"
      } else if (resType == "Metal" && type =="Rec") {
        color = "#999"
      } else if (resType == "Crystal" && type =="Rec") {
        color = "#f9f"
      }
      if (resType != "Deuterium" || type != "Rec") {
        datasets.push({
          label: resType,
          type : type,
          data,
          backgroundColor: [color]
        })
      }
    }
  }

let labels = uniqueDates
const data = {
  labels,
  datasets: [
    {
      label: 'Met Raid',
      data: datasets[0]['data'],
      backgroundColor: datasets[0]['backgroundColor'][0],
      stack: 'Stack 0',
    },
    {
      label: 'Met Rec',
      data: datasets[1]['data'],
      backgroundColor: datasets[1]['backgroundColor'][0],
      stack: 'Stack 0',
    },
    {
      label: 'Cris Raid',
      data: datasets[2]['data'],
      backgroundColor: datasets[2]['backgroundColor'][0],
      stack: 'Stack 1',
    },
    {
      label: 'Cris Rec',
      data: datasets[3]['data'],
      backgroundColor: datasets[3]['backgroundColor'][0],
      stack: 'Stack 1',
    },
    {
      label: 'Deut Raid',
      data: datasets[4]['data'],
      backgroundColor: datasets[4]['backgroundColor'][0],
      stack: 'Stack 2',
    }
  ]
}
return data
}

function draw(usecase) {
  // console.log("Drawing usecase: ", usecase)

  let data, chartType, isStacked = true
  switch (usecase) {
    case "dailyRes":
      data = getChartDataDailyRes()
      chartType = "bar"
      isStacked = false
      break
    case "dailyRec":
      data = getChartDataDailyRec()
      chartType = "bar"
      isStacked = false
      break
    case "dailyAll":
      data = getChartDataDailyAll()
      chartType = "bar"
      isStacked = true
      break
  }

  if (chartType != chartObj.type) {
    chartObj.destroy()
    chartObj = createChart(chartType)
  }
  chartObj.data = data
  chartObj.options.scales.x.stacked = isStacked
  chartObj.options.scales.y.stacked = isStacked
  if(GM_getValue("log")) {
    chartObj.options.scales.y.type = 'logarithmic'
  }
  chartObj.update()
}

if (ep.isRaidPage()) {
  updateRaidDB()
  addChart()
  draw(document.getElementById("chartSelect").value)
}
if (ep.isRecPage()) {
  updateRecDB()
}