/* globals shortly_number */
import {GM_getValue, GM_setValue} from "./general";

function add_to_db(key, size, extra) {
    const expodb = GM_getValue("lp_expo_counts", {
        "Res": [0, 0, 0],
        "Pirates": [0, 0, 0],
        "Aliens": [0, 0, 0],
        "Speed": [0, 0],
        "Lost": [0],
        "Ships": [0, 0, 0],
        "Nothing": [0]
    })
    const ship_res_db = GM_getValue("lp_expo_ship_res", {"Res": {'901':0,'902': 0, '903':0}, "Ships": {}})

    expodb[key][size]+=1

    if(key==="Res" || key==="Ships"){
        for(let k in extra){
            if(!(k in ship_res_db[key])){
                ship_res_db[key][k]=0
            }
            ship_res_db[key][k] +=extra[k]
        }
    }


    GM_setValue("lp_expo_counts", expodb)
    GM_setValue("lp_expo_ship_res", ship_res_db)
}



export function selectmesages() {
    for(let x of document.querySelectorAll("div[name='expoinfo']")){
        showexpo(x)
    }


}


function gen_obj(ttx, key) {
    const robj = {ttl: 0, size: ttx[key]}
    for (let v of ttx[key]) {
        robj.ttl += v
    }
    return robj
}


function showexpo(div){
    const expo_ids = GM_getValue("lp_expo_handled_ids", {})
    let id= div.parentNode.parentNode.className.split(" ")[0]
    let showing=document.createElement("span")
    const colors={
        "Pirates":'#FFFF00',
        "Res": '#90EE90',
        "Ships" :  '#90EE90',
        "Lost" : "#eb34c9",
        "Speed":'#FFFF00',
        "Nothing":"#FFFFFF",
        "Aliens" : '#FF0000'
    }
    const expsizes={
        "Fast":0,
"Slow":1,
"Small":0,
"Medium":1,
"Big":2,
        "":0
    }
    let expoevent=div.getAttribute("expevent")
    let expsize=div.getAttribute("expsize")

    showing.innerHTML =expoevent + ":" + expsize

    showing.style["font-weight"]="bold"
    showing.style.color=   colors[div.getAttribute("expevent") ]
    div.parentElement.appendChild(document.createElement("br"))
    if(expoevent==="Speed"){
        showing.innerHTML +=" | factor:" + JSON.parse(div.getAttribute("expvalue"))['factor'] + "x"
    }
if(!(id in expo_ids)){
    if(expoevent==="Res" || expoevent==="Ships") {
        let extra=JSON.parse(div.getAttribute("expvalue"))
        add_to_db(expoevent,expsizes[expsize],extra)
    }else{
        add_to_db(expoevent,expsizes[expsize])

    }
    expo_ids[id]=1
    GM_setValue("lp_expo_handled_ids", expo_ids)
}

    div.parentElement.appendChild(showing)
}


export function expo_table() {
    const expodb = GM_getValue("lp_expo_counts", {
        "Res": [0, 0, 0],
        "Pirates": [0, 0, 0],
        "Aliens": [0, 0, 0],
        "Speed": [0, 0],
        "Lost": [0],
        "Ships": [0, 0, 0],
        "Nothing": [0]
    })
    const ship_res_db = GM_getValue("lp_expo_ship_res", {"Res": {'901':0,'902': 0, '903':0}, "Ships": {}})


    const res = gen_obj(expodb, "Res")
    res.met = ship_res_db.Res["901"]
    res.krist = ship_res_db.Res["902"]
    res.deut = ship_res_db.Res["903"]
    const pirate = gen_obj(expodb, "Pirates")
    const alien = gen_obj(expodb, "Aliens")
    const ships = gen_obj(expodb, "Ships")
    const speed = gen_obj(expodb, "Speed")
    const hole = gen_obj(expodb, "Lost")
    const nothing = gen_obj(expodb, "Nothing")
    let ttl = nothing.ttl + hole.ttl + speed.ttl + ships.ttl + alien.ttl + pirate.ttl + res.ttl
    let txtr = `
           <table>
        <tr>
          <th>Event</th>
          <th>Total ${ttl}</th>
          <th>Percent</th>
        </tr>
          <tr>
          <td>Res<br>s:${res.size[0]} <br>m:${res.size[1]}<br>b:${res.size[2]}</td>
          <td>${res.ttl}<br>met:${shortly_number(res.met)}<br>krist:${shortly_number(res.krist)}<br>deut:${shortly_number(res.deut)}</td>
          <td>${Math.round(res.ttl * 1000 / ttl) / 10}<br>met:${shortly_number(res.met / (res.ttl + 0.00001))}<br>krist:${shortly_number(res.krist / (res.ttl + 0.00001))}<br>deut:${shortly_number(res.deut / (res.ttl + 0.00001))}</td>
        </tr>
         <tr>
          <td>Ships <br>s:${ships.size[0]} <br>m:${ships.size[1]}<br>b:${ships.size[2]}</td>
          <td>${ships.ttl}</td>
          <td>${Math.round(ships.ttl * 1000 / ttl) / 10}</td>
        </tr>

        <tr>
          <td>Pirates <br>s:${pirate.size[0]} <br>m:${pirate.size[1]}<br>b:${pirate.size[2]}</td>
          <td>${pirate.ttl}</td>
          <td>${Math.round(pirate.ttl * 1000 / ttl) / 10}</td>
        </tr>
        <tr>
          <td>Aliens <br>s:${alien.size[0]} <br>m:${alien.size[1]}<br>b:${alien.size[2]}</td>
          <td>${alien.ttl}</td>
          <td>${Math.round(alien.ttl * 1000 / ttl) / 10}</td>
        </tr>
        <tr>
          <td>speed <br>fast:${speed.size[0]} <br>slow:${speed.size[1]}</td>
          <td>${speed.ttl}</td>
          <td>${Math.round(speed.ttl * 1000 / ttl) / 10}</td>
        </tr>
        <tr>
          <td>nixname</td>
          <td>${hole.ttl}</td>
          <td>${Math.round(hole.ttl * 1000 / ttl) / 10}</td>
        </tr>
          <td>nichts</td>
          <td>${nothing.ttl}</td>
          <td>${Math.round(nothing.ttl * 1000 / ttl) / 10}</td>
        </tr>

      </table>
    `

    let expo_ships = ship_res_db.Ships
    let stable = `
                  <table>
        <tr>
          <th>shiptype</th>
          <th>Amount</th>
        </tr>`
    const shipnames={
        202:"Kleiner Transporter",
        203:"Großer Transporter",
        204:"Leichter Jäger",
        205:"Schwerer Jäger",
        206:"Kreuzer",
        207:"Schlachtschiff",
        210:"Spionagesonde",
        211:"Bomber",
        213:"Zerstörer",
        215:"Schlachtkreuzer"
    }

  for (let stype in expo_ships) {
        stable += `
                        <tr>
          <td>${shipnames[stype]}</td>
          <td>${expo_ships[stype]}</td>
        </tr>
            `
    }
    stable += "    </table>"

    return txtr + stable

}
