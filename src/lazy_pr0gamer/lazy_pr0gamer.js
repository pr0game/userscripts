'use strict';
import {GM_setValue, GM_getValue, get_sortet_planet_list} from "./general"
import {marketstuff} from "./market";
import {
    calcfull,
    calctime,
    saveres,
    set_planets,
    show_prodph,
    update_current_res,
    update_production
} from "./resources";
import {
    commander_building_reader, commander_building_table,
    commander_def_reader, commander_def_table, commander_fleet_table,
    commander_res_table,
    get_imperiom_stuff,
    save_planet_fleet
} from "./imperium";
import {expo_table, selectmesages} from "./expos";
import {debris_text} from "./galaxy";

function gen_fieldset(id, func, name) {
    const fields = GM_getValue("menu_vis", {})
    return `<fieldset ><legend style="float:right"><a  id="${id}" href="#" onclick="var spoilernode = this.parentNode.parentNode.lastChild; if (spoilernode.style.display === 'block') spoilernode.style.display='none'; else spoilernode.style.display = 'block'; return false;">${name}</a></legend><div style="display: ${fields[id]}" class="tscroll">${func()}</div></fieldset>`
}

function addstuff(side_nodes) {
    const planets_selected = GM_getValue("select", {})
    var psel_str = ""
    for (let p of get_sortet_planet_list(Object.keys(planets_selected))) {
        let checked = ""
        if (planets_selected[p] === 1) {
            checked = "checked"
        }
        psel_str += '<input type="checkbox" id="' + p + '"  ' + checked + ' >[' + p + '<span id="full_' + p + '"></span><br>'
    }

    var div = document.createElement("div");
    div.style.cssText = ' grid-row: 2; grid-column: 1;    overflow-x: hidden;width: 20vw;'
    var ct = document.createElement("center");
    ct.id = "atain"
    div.className = "no-mobile";
    div.appendChild(ct)
    document.getElementsByClassName("wrapper")[0].appendChild(div)
    var li = document.createElement("li")
    li.style.cssText = "    overflow: hidden;    font-size: 12px;    background: #2a2e31;"
    li.id = "disc2"
    li.innerHTML = `
M:<input placeholder="met" id="t_met" type="number" step="1000" ><br>
K:<input placeholder="krist" id="t_krist" type="number" step="1000"><br>
D:<input placeholder="deut" id="t_deut" type="number" step="1000">
<br>
current:<br>M:
<span id="c_met"></span><br>K:<span id="c_krist"></span><br>D:<span id="c_deut"></span>
<br>
prod:
<span id="p_met"></span>,<span id="p_krist"></span>,<span id="p_deut"></span>
<br>
time:
<span id="time_til_got"></span>
<br>

` + psel_str

    document.getElementById("menu").appendChild(li)
    var ihtml = ""
    for (let node of side_nodes) {
        ihtml += gen_fieldset(node.id, node.table, node.name)
    }

    document.getElementById("atain").innerHTML = ihtml;
    document.getElementById("t_met").addEventListener("change", saveres);
    document.getElementById("t_krist").addEventListener("change", saveres);
    document.getElementById("t_deut").addEventListener("change", saveres);
    for (let node of side_nodes) {
        document.getElementById(node.id).addEventListener("click", change_menu);
        if (node.func !== null) {
            node.func();
        }
    }

    const resstatus = GM_getValue("res_input", [0, 0, 0])
    document.getElementById("t_met").value = resstatus[0]
    document.getElementById("t_krist").value = resstatus[1]
    document.getElementById("t_deut").value = resstatus[2]
    let a = document.createElement("div")
    let b = document.createElement("div")
    let c = document.createElement("div")
    a.style.cssText = 'font-size: 11px;color:#0F0'
    b.style.cssText = 'font-size: 11px;color:0F0'
    c.style.cssText = 'font-size: 11px;color:0F0'
    a.innerHTML = "<span style='color:yellow' id='met_top'></span> <span id='met_top_p' style='color:#0F0'></span>"
    b.innerHTML = "<span style='color:yellow' id='krist_top'></span> <span id='krist_top_p' style='color:#0F0'></span>"
    c.innerHTML = "<span style='color:yellow' id='deut_top'></span> <span id='deut_top_p' style='color:#0F0'></span>"

    document.getElementsByClassName("no-mobile")[4].appendChild(a)
    document.getElementsByClassName("no-mobile")[6].appendChild(b)
    document.getElementsByClassName("no-mobile")[8].appendChild(c)
    for (let n of document.getElementById("disc2").querySelectorAll('input[type="checkbox"]')) {
        n.addEventListener('change', planet_select_change);
    }
}

function change_menu() {
    const cm = GM_getValue("menu_vis", {})
    let id = this.id
    if (cm[id] === "none") {
        cm[id] = "block"
    } else {
        cm[id] = "none"
    }
    GM_setValue("menu_vis", cm);
}


function planet_select_change() {
    const planets_selected = GM_getValue("select", {})
    planets_selected[this.id] = Math.abs(planets_selected[this.id] - 1)
    GM_setValue("select", planets_selected)
    show_prodph();
    calctime();
}

function remove_old_planets() {
    let planets = document.getElementById("planetSelector")
    if (planets == null) {
        return;
    }
    let pids = {}
    for (let p of planets.children) {
        let pname = p.innerText.split("[")[1]
        if (p.innerText.includes("(")) {
            pname = pname + "M"
        }
        pids[pname] = 1
    }
    const gmnames = ["commander_building",
        "res",
        "prod",
        "commander_fleet",
        "commander_def",
    "select"]
    for (let gmv of gmnames) {
        let mvx = GM_getValue(gmv, {})
        let rmv = []
        for (let k in mvx) {
            if (pids[k] !== 1) {
                rmv.push(k)
            }
        }
        for (let k of rmv) {
            delete mvx[k];
        }
        GM_setValue(gmv, mvx)
    }
}

remove_old_planets();


function add_talbe_scroll() {

    var style = document.createElement('style');
    style.type = 'text/css';
    if (style.styleSheet) {
        style.styleSheet.cssText = `.tscroll {
  width: 100%;
  overflow-x: auto;
}

.tscroll table *:first-child {
  position: sticky;
  left: 0;
}
`;
    } else {
        style.appendChild(document.createTextNode(`.tscroll {
    width: 20vw;
  overflow-x: auto;
}

.tscroll table *:first-child {
  position: sticky;
  left: 0;
}
`));
    }
    document.getElementsByTagName('head')[0].appendChild(style);


}




let modules = [{id: "custom_commander_res", table: commander_res_table, name: "Commander Res", func: null},
    {id: "custom_commander_building", table: commander_building_table, name: "Commander Build", func: null},
    {id: "custom_commander_fleet", table: commander_fleet_table, name: "Commander Fleet", func: null},
    {id: "custom_commander_def", table: commander_def_table, name: "Commander Def", func: null},
    {id: "expo_menu", table: expo_table, name: "Expo statistik", func: null},
]



marketstuff();

add_talbe_scroll();


//imperium

get_imperiom_stuff();
commander_def_reader();
commander_building_reader();
save_planet_fleet();


selectmesages(); //expo



//res
addstuff(modules);
debris_text()
set_planets();
update_current_res();
update_production();
show_prodph();
calcfull();
calctime();
setInterval(calcfull, 1000);






