import {pad} from "./general";

export function show_phalanx_exact() {
    if (!window.location.href.includes("page=phalanx")) {
        return
    }

    const mh = document.getElementsByClassName("fleets")
    for (let flight of mh) {
        let nd = new Date(parseInt(flight.getAttribute("data-fleet-end-time")) * 1000);
        flight.parentNode.innerHTML = '<span style="color:yellow">' + nd.getHours() + ":" + pad(nd.getMinutes(), 2) + ":" + pad(nd.getSeconds(), 2) + ' </span>| ' + flight.parentNode.innerHTML
    }


}

