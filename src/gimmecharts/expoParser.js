'use strict';
class ExpoParser {

  constructor() {
    this.expoTypeMessages = {
      // see expoMessageHashes.js file to generate this
      "pirates_s": [-1221864748, 231627849, -81668849, -189730544, -306163214, 122214509, 1785611436, -631671739, 296173556, -899673140],
      "pirates_m": [-122157721, 14935103, 2141671458, 1993851820, -2072130467, 174650377],
      "pirates_l": [12580367, -1752110115, -988485896, -409779357, -361016826, 1587837379],
      "aliens_s": [-1520199567, 1584923285, 690823421, -907891375, -3604301, -1148608318, -1931424770, -566186986],
      "aliens_m": [1389658151, 1086698980, -314610216, -1756694473, -1316384243, 1614353879],
      "aliens_l": [369136588, 525276960, -1317875709, -1155605430, 83819078, 144685103],
      "res_s": [772721036, 650305384, -2037315790, -1925830192, 1919990082, 1635171070, -1718269928],
      "res_m": [849438408, -1304825103, -1131882351, -500249065, 1657352286, -2073530039],
      "res_l": [1341935694, 125540507, 909910013, -458865384],
      "ship_s": [416881200, 742242619, 249179935, -90021707, -1117356138, 156638633, -1602732192, -2083470006],
      "ship_m": [-397440342, -673244391, 1108671124, -572778267],
      "ship_l": [30046738, 503520173, 381587845, -1051808747],
      "speed_fast": [1097505031, 488460138, -473408812, -1853744199, -970179665, 1630145342],
      "speed_slow": [-835509668, -762096937, 434645185, 426467124, 413832516, 241477053, 1219022925, -331280140, 1899425994, 1783236000, -1677842983, -441583177],
      "lost": [1728339926, 181161853, -1438446270, 423800420, 460900785, -1374399479, 1635501239, -2063188370],
      "nothing": []
    }
  }
  isExpoPage() {
    return window.location.search.includes("page=messages") && window.location.search.includes("category=15")

  }
  getMessages() {
    const htmlElements = document.getElementsByClassName("message_head")
    const messages = []
    for (const m of htmlElements) {
      if (!m) return
      const id = parseInt(m.id.split("_")[1])
      const header = m.innerText.trim()
      const body = document.getElementsByClassName(`message_${id} messages_body`)[0].innerText.trim()
      messages.push({
        id,
        header,
        body
      })
    }
    return messages
  }
  parse_text({
    id,
    header,
    body
  }) {
    function parseDate(dateRaw) {
      const monthMapping = languageMap.months
      // make sure date format is english
      for (let monthDE in monthMapping) {
        dateRaw = dateRaw.replace(monthDE, monthMapping[monthDE])
      }
      // move around day so it can be parsed
      const parts = dateRaw.split(" ")
      const date = new Date([parts[1], parts[0].replaceAll(".", ""), parts.slice(2)].join(" "))
      return date
    }

    function parsePlanet(report) {
      return report.split("[")[1].split("]")[0]
    }

    function parseType(expoMessage, expoTypeMessages) {
      for (const type in expoTypeMessages) {
        const hashedMessage = ExpoParser.hashText(expoMessage)
        if (expoTypeMessages[type].indexOf(hashedMessage) >= 0) {
          return type
        }
      }
      return "nothing"
    }

    function parseDetails(rest, expoType) {
      if (expoType.includes("res_")) {
        const sizeRaw = rest[0]
        const parts = sizeRaw.split(" ")
        const size = {
          count: 0,
          type: ""
        }
        if (sizeRaw.includes("abgebaut.")) {
          // german
          size.count = parseInt(parts[2].replaceAll(".", "").replaceAll(",", ""))
          size.type = parts[3].replaceAll("Kristall", "Crystal").replaceAll("Metall", "Metal")
        } else if (sizeRaw.includes("mined.")) {
          // english
          size.count = parseInt(parts[0].replaceAll(".", "").replaceAll(",", ""))
          size.type = parts[1]
        }
        return size
      } else if (expoType.includes("ship_")) {
        const lines = rest[0].split(/\n/)
        const found = {}
        for (const l of lines.slice(1)) {
          const [ship, count] = l.split(": ")
          let shipShort = ship
          // switch from longname "Battle Cruiser" to shortname "bcr"
          for (const map of [languageMap.ships_en, languageMap.ships_de]) {
            const index = Object.keys(map).indexOf(ship)
            if (index > -1) {
              shipShort = Object.values(map)[index]
              break
            }
          }
          found[shipShort] = parseInt(count)
        }
        return found
      } else if (expoType.includes("pirates_") || expoType.includes("aliens_")) {
        const losses = rest[0].split(/\n/)[2].split(/\t/)[1]
        const lostShips = parseInt(losses.split(": ")[2].replace(".", "").replace(",", ""))
        return lostShips
      }
      return ""
    }
    const [dateRaw, ] = header.split(/\t/)
    const [report, expoMessage, ...rest] = body.split(/\n\n/)
    const date = parseDate(dateRaw).toISOString()
    const planet = parsePlanet(report)
    const expoType = parseType(expoMessage, this.expoTypeMessages)
    const details = parseDetails(rest, expoType)
    return {
      id,
      date,
      planet,
      expoType,
      details
    }
  }

  static hashText(text) {
    // taken from https://stackoverflow.com/a/8831937
    let hash = 0
    for (var i = 0; i < text.length; i++) {
      var char = text.charCodeAt(i)
      hash = ((hash << 5) - hash) + char
      hash = hash & hash
    }
    return hash
  }
}

const languageMap = {
  months: {
    "Jan": "Jan",
    "Feb": "Feb",
    "Mär": "Mar",
    "Apr": "Apr",
    "Mai": "May",
    "Jun": "Jun",
    "Jul": "Jul",
    "Aug": "Aug",
    "Sep": "Sep",
    "Okt": "Oct",
    "Nov": "Nov",
    "Dez": "Dec"
  },
  ships_en: {
    "Light Cargo": "lc",
    "Heavy Cargo": "hc",
    "Light Fighter": "lf",
    "Heavy Fighter": "hf",
    "Cruiser": "cr",
    "Battleship": "bs",
    "Colony Ship": "col",
    "Recycler": "rec",
    "Spy Probe": "spy",
    "Planet Bomber": "bomb",
    "Solar Satellite": "sat",
    "Star Fighter": "dest",
    "Battle Fortress": "rip",
    "Battle Cruiser": "bcr"
  },
  ships_de: {
    "Kleiner Transporter": "lc",
    "Großer Transporter": "hc",
    "Leichter Jäger": "lf",
    "Schwerer Jäger": "hf",
    "Kreuzer": "cr",
    "Schlachtschiff": "bs",
    "Kolonieschiff": "col",
    "Recycler": "rec",
    "Spionagesonde": "spy",
    "Bomber": "bomb",
    "Solarsatellit": "sat",
    "Zerstörer": "dest",
    "Todesstern": "rip",
    "Schlachtkreuzer": "bcr"
  }
}

module.exports = {
  ExpoParser,
  languageMap
}