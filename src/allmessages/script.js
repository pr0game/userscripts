'use strict';
/* globals */
function reassignMessageButtons() {
  const setNewUrl = a => a.setAttribute('href', 'game.php?page=messages&category=100')
  const popup = document.querySelector('div.message a')
  if (popup) {
    setNewUrl(popup)
  }
  const menuButton = Array.from(document.querySelectorAll('ul#menu a')).find(e => e.href.includes('page=messages'))
  setNewUrl(menuButton)
}

reassignMessageButtons()